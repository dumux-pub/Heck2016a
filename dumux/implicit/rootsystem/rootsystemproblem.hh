// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for all fully implicit Rootsystem problems
 */
#ifndef DUMUX_ROOTSYSTEM_PROBLEM_HH
#define DUMUX_ROOTSYSTEM_PROBLEM_HH

#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/implicit/growth/gridgrowth.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>

#include "rootsystemproperties.hh"

namespace Dumux
{
/*!
 * \ingroup RootsystemModel
 * \ingroup ImplicitBaseProblems
 * \brief Base class for all fully implicit Rootsystem problems
 *
 * For a description of the Rootsystem model, see Dumux::RootsystemModel
 */
template<class TypeTag>
class RootsystemProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Intersection Intersection;

    enum {
        // Grid and world dimension
        dimWorld = GridView::dimensionworld
    };
    enum { growingGrid = GET_PROP_VALUE(TypeTag, GrowingGrid) };

    typedef ImplicitGridGrowth<TypeTag, growingGrid> GridGrowthModel;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;



public:
    /*!
     * \brief The constructor.
     *
     * The overloaded class must allocate all data structures
     * required, but _must not_ do any calls to the model, the
     * jacobian assembler, etc inside the constructor.
     *
     * If the problem requires information from these, the
     * ImplicitProblem::init() method be overloaded.
     *
     * \param timeManager The TimeManager which keeps track of time
     * \param gridView The GridView used by the problem.
     */
    RootsystemProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView), vtkWriter(this->gridView(), "pressure", ".", "")
    {
        waterStress_ = false;
        switchBC_ = false;

        // if we are calculating on an growing grid get the grid growth model
        if (growingGrid)
            gridGrowth_ = Dune::make_shared<GridGrowthModel>(*static_cast<Implementation*>(this));

    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        ParentType::init();

        if (growingGrid)
        {
            gridGrowth().init();
        }

        Scalar t = this->timeManager().time() +  this->timeManager().timeStepSize();
        int vecSize = this->gridView().size(0);
        pressure.resize(vecSize);
        vtkWriter.addCellData(pressure, "celldata");
        vtkWriter.write(t);

    }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void preTimeStep()
    {
        // If gridGrowth is used, this method grows the grid.
        // Remeber to call the parent class function if this is overwritten
        // on a lower problem level when using an adaptive grid
        //if (growingGrid && this->timeManager().timeStepIndex() > 0)
        this->gridGrowth().growGrid();
        this->resultWriter().gridChanged();

        //int vecSize = this->gridView().size(0);
        //pressure.resize(vecSize);

        //const SolutionVector& curSol = this->model().curSol();
        //Scalar t = this->timeManager().time() +  this->timeManager().timeStepSize();

       /* ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt){
            int eIdx = this->model().elementMapper().index(*eIt);
            std::cout << "element no: " << eIdx
                      << " element center: " << eIt->geometry().center()[2]
                      << " element volume: " << eIt->geometry().volume()
                      << std::endl;

            //pressure[eIdx] = curSol[eIdx];
                } */

        //Dune::VTKWriter<GridView > writer(this->gridView(), Dune::VTK::nonconforming);
        //writer.addCellData(pressure, "celldata");
        //writer.write("pressure");

        //vtkWriter(this->gridView(), "pressure", ".", "");
        //vtkWriter.addCellData(pressure, "celldata");
    }

    void postTimeStep()
    {
        int vecSize = this->gridView().size(0);
        pressure.resize(vecSize);

        const SolutionVector& curSol = this->model().curSol();
        Scalar t = this->timeManager().time() +  this->timeManager().timeStepSize();

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt){
            int eIdx = this->model().elementMapper().index(*eIt);
            pressure[eIdx] = curSol[eIdx];
        }

        vtkWriter.write(t);
    }
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scvIdx The local subcontrolvolume index
     * \param elemVolVars All volume variables for the element
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &values,
                     const Element &element,
                     const FVElementGeometry &fvGeometry,
                     const int scvIdx,
                     const ElementVolumeVariables &elemVolVars) const
    {
        const SpatialParams &spatialParams = this->spatialParams();
        Scalar Kr = spatialParams.Kr(element, fvGeometry, scvIdx);
        Scalar rootSurface = spatialParams.rootSurface(element, fvGeometry, scvIdx);

        Scalar phx = elemVolVars[0].pressure();
        Scalar phs = GET_RUNTIME_PARAM(TypeTag, Scalar, BoundaryConditions.SoilPressure);
        values = Kr * rootSurface * ( phs - phx) / element.geometry().volume();

    }

    void setWaterStress()
    {
        const SolutionVector& curSol = this->model().curSol();
        FVElementGeometry fvGeometry;
        bool waterStress_old = waterStress_;
        double Hcrit = GET_RUNTIME_PARAM(TypeTag,
                                         Scalar,
                                         BoundaryConditions.CriticalCollarPressure);

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt){

            fvGeometry.update(this->gridView(), *eIt);

            for (unsigned int i=0; i<eIt->geometry().corners();i++){
                GlobalPosition globalPos = eIt->geometry().corner(i);

                if ( globalPos[2] < eps_ ) {
                    if( curSol[0] <=  Hcrit){
                        waterStress_ = true;
                    }
                    else
                        waterStress_ = false;
                }
            }
        }
        if (waterStress_ !=  waterStress_old)
            switchBC_= true;
    }

    bool getWaterStress() const
    {
        return waterStress_;
    }

    void resetSwitchBC()
    {
       switchBC_= false;
    }

    bool getSwitchBC()
    {
       return switchBC_;
    }

    /*!
     * \brief Returns growth model used for the problem.
     */
    GridGrowthModel& gridGrowth()
    {
        return *gridGrowth_;
    }

    /*!
     * \brief Returns growth model used for the problem.
     */
    const GridGrowthModel& gridGrowth() const
    {
        return *gridGrowth_;
    }

    /*!
     * \brief Capability to introduce problem-specific routines at the
     * beginning of the grid growth
     *
     * Function is called at the beginning of the standard grid
     * modification routine, GridGrowth::growGrid(.
     */
    void preGrowth()
    {}

    /*!
     * \brief Capability to introduce problem-specific routines after grid growth
     *
     * Function is called at the end of the standard grid
     * modification routine, GridGrowth::growGrid() , to allow
     * for problem-specific output etc.
     */
    void postGrowth()
    {}

private:
    bool waterStress_ = false;
    bool switchBC_ = false;
    const double eps_ = 1e-6;
    Dune::shared_ptr<GridGrowthModel> gridGrowth_;
    Dune::VTKSequenceWriter<GridView> vtkWriter;
    std::vector<Scalar> pressure;
};
}

#include <dumux/implicit/growth/gridgrowthpropertydefaults.hh>

#endif
