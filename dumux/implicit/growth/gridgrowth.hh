// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for h-adaptive implicit models.
 */
#ifndef DUMUX_IMPLICIT_GRIDGROWTH_HH
#define DUMUX_IMPLICIT_GRIDGROWTH_HH

#include "gridgrowthproperties.hh"
#include "growthhelper.hh"
#include <unordered_map>

#include <dune/common/exceptions.hh>

namespace Dumux
{

/*!\ingroup ImplicitGridGrowth
 * @brief Standard Module for h-adaptive simulations
 *
 * This class is created by the problem class with the template
 * parameters <TypeTag, true> and provides basic functionality
 * for adaptive methods:
 *
 * A standard implementation adaptGrid() will prepare everything
 * to calculate the next primary variables vector on the new grid.
 */
template<class TypeTag, bool growingGrid>
class ImplicitGridGrowth
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar)   Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem)  Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GridView::Grid Grid;
    typedef typename Grid::LeafGridView LeafGridView;
    typedef typename LeafGridView::template Codim<0>::Iterator LeafIterator;
    typedef typename GridView::IntersectionIterator LeafIntersectionIterator;
    typedef typename Grid::template Codim<0>::Entity Element;
    typedef typename Grid::template Codim<0>::EntityPointer ElementPointer;

    typedef typename GET_PROP_TYPE(TypeTag, GrowthIndicator) GrowthIndicator;

    enum {dimWorld = GridView::dimensionworld};
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * Constructor for h-adaptive simulations (adaptive grids)
     * @param problem The problem
     */
    ImplicitGridGrowth (Problem& problem)
        : growthHelper_(problem.gridView()),
          problem_(problem),
          growthIndicator_(problem),
          grew_(0),
          removed_(0),
          merged_(0)
    {
    }

    /*!
     * @brief Initalization method of the h-adaptive module
     *
     * Prepares the grid for simulation after the initialization of the
     * problem. The applied indicator is selectable via the property
     * AdaptationInitializationIndicator
     */
    void init()
    {
        growthIndicator_.init();
        problem_.model().init(problem_);
    }

    /*!
     * @brief Standard method to adapt the grid
     *
     * This method is called from IMPETProblem::preTimeStep() if
     * adaptive grids are used in the simulation. It uses the standard
     * indicator (selected by the property AdaptationIndicator) and forwards to
     * with it to the ultimate method adaptGrid(indicator), which
     * uses a standard procedure for adaptivity:
     * 1) Determine the refinement indicator
     * 2) Mark the elements
     * 3) Store primary variables in a map
     * 4) Adapt the grid, adapt variables sizes, update mappers
     * 5) Reconstruct primary variables, regain secondary variables
     */
    void growGrid()
    {
        growGrid(growthIndicator_);
    }

    /*!
     * @brief Method to adapt the grid with individual indicator vector
     *
     * @param indicator The refinement indicator that is applied
     *
     * This method is called by an user-defined preTimeStep() of
     * the applied problem and takes a given vector with indicator
     * values.
     *
     * It uses a standard procedure for adaptivity:
     * 1) Determine the refinement indicator
     * 2) Mark the elements
     * 3) Store primary variables in a map
     * 4) Adapt the grid, adapt variables sizes, update mappers
     * 5) Reconstruct primary variables, regain secondary variables
     */
    template<class Indicator>
    void growGrid(Indicator& indicator)
    {
        // reset internal counter for marked elements
        grew_ = removed_ = merged_ = 0;

        // check for adaptation interval: Adapt only at certain time step indices
        //if (problem_.timeManager().timeStepIndex() % growthInterval_ != 0)
        //    return;

        /**** 1) determine refining parameter if standard is used ***/
        // if not, the indicatorVector and refinement Bounds have to
        // specified by the problem through setIndicator()
        indicator.calculateIndicator();

        /**** 2) mark elements according to indicator     *********/
        markElements(indicator);

        // abort if nothing in grid is marked
        int sumGrew = problem_.grid().comm().sum(grew_);
        int sumRemoved = problem_.grid().comm().sum(removed_);
        if (sumGrew == 0 && sumRemoved == 0)
            return;
        else
            Dune::dinfo << grew_ << " cells have been grew_ to be refined, "
                        << removed_ << " to be coarsened." << std::endl;

        /****  2b) Do pre-adaptation step    *****/
        problem_.grid().preGrow();
        //problem_.preGrow();

        /****  3) Put primary variables in a map         *********/
        growthHelper_.storePrimVars(problem_);

        /****  4) Grow Grid and size of variable vectors    *****/
        problem_.grid().grow();

        // update mapper to new cell indices
        problem_.elementMapper().update();
        problem_.vertexMapper().update();

        // adapt size of vectors
        problem_.model().adaptVariableSize();

        /****  5) (Re-)construct primary variables to new grid **/
        growthHelper_.reconstructPrimVars(problem_);

        // delete markers in grid
        problem_.grid().postGrow();

        // call method in problem for potential output etc.
        //problem_.postGrow();

        return;
    }

    /*!
     * Mark Elements for grid refinement according to applied Indicator
     * @param indicator The refinement indicator that is applied
     */
    template<class Indicator>
    void markElements(Indicator& indicator)
    {
        for (LeafIterator eIt = problem_.gridView().template begin<0>();
             eIt!=problem_.gridView().template end<0>(); ++eIt)
        {
            // only mark non-ghost elements
            if (eIt->partitionType() != Dune::GhostEntity)
            {
                // grow?
                if (indicator.markedForGrowth(*eIt))
                {
                    GlobalPosition growthPoint = indicator.getNewPoint(*eIt);
                    problem_.grid().markForGrowth(*eIt, indicator.getGrowthFacetIndex(*eIt),growthPoint );
                    ++grew_;
                }

                if (indicator.markedForRemoval(*eIt))
                {
                    //problem_.grid().markForRemoval(*eIt);
                    //++removed_;
                }

                if (indicator.markedForMerging(*eIt))
                {
                    //problem_.grid().markForMerging(*eIt, indicator.getMergingFacetIndex(*eIt), indicator.getConnectingEntity(*eIt), indicator.getConnectingFacetIndex(*eIt));
                    //++merged_;
                }
            }
        }
    }

    /*!
     * @brief Returns true if grid cells have been marked for adaptation
     */
    bool wasGrown()
    {
        int sumGrew = problem_.grid().comm().sum(grew_);
        int sumRemoved = problem_.grid().comm().sum(removed_);

        return (sumGrew != 0 || sumRemoved != 0);
    }

    const bool wasGrown() const
    {
        int sumGrew = problem_.grid().comm().sum(grew_);
        int sumRemoved = problem_.grid().comm().sum(removed_);

        return (sumGrew != 0 || sumRemoved != 0);
    }

    GrowthIndicator& growthIndicator()
    {
        return growthIndicator_;
    }

    GrowthIndicator& growthIndicator() const
    {
        return growthIndicator_;
    }

private:
    GrowthHelper<TypeTag> growthHelper_;
    Problem& problem_;
    GrowthIndicator growthIndicator_;
    int grew_;
    int removed_;
    int merged_;
    int growthInterval_;
};

/*!
 * @brief Class for simulations without grid growth
 *
 * This class provides empty methods for simulations without grid growth
 * for compilation reasons. If grid growth is desired, create the
 * class with template arguments <TypeTag, true> instead.
 */
template<class TypeTag>
class ImplicitGridGrowth<TypeTag, false>
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

public:
    void init()
    {}
    void growGrid()
    {}
    bool wasGrown()
    {
        return false;
    }
    ImplicitGridGrowth (Problem& problem)
    {}
};

}
#endif /* DUMUX_IMPLICIT_GRIDGROWTH_HH */
