// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GROWTHHELPER_HH
#define DUMUX_GROWTHHELPER_HH

#include <dune/grid/utility/persistentcontainer.hh>

/**
 * @file
 * @brief  Base class holding the variables for implicit models.
 */

namespace Dumux
{

template<class TypeTag>
class GrowthHelper
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    struct GrowthValues
    {
        PrimaryVariables u;
        std::vector<double> spatialVars;
        int count;
        GrowthValues()
        {
            count = 0;
            spatialVars.resize(8);
        }
    };

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef typename GridView::Grid Grid;
    typedef typename Grid::LevelGridView LevelGridView;
    typedef typename LevelGridView::template Codim<dofCodim>::Iterator LevelIterator;
    typedef typename LevelGridView::template Codim<0>::Iterator ElementLevelIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef Dune::PersistentContainer<Grid, GrowthValues> PersistentContainer;

    const GridView gridView_;
    const Grid& grid_;
    PersistentContainer growthMap_;
    PrimaryVariables priVars_;

public:
    //! Constructs an adaptive helper object
    /**
     * In addition to providing a storage object for cell-centered Methods, this class provides
     * mapping functionality to adapt the grid.
     *
     *  @param gridView a DUNE gridview object corresponding to diffusion and transport equation
     */
    GrowthHelper(const GridView& gridView) :
        gridView_(gridView), grid_(gridView.grid()), growthMap_(grid_, dofCodim)
    {}


    /*!
     * Store primary variables
     *
     * To reconstruct the solution in father elements, problem properties might
     * need to be accessed.
     * From upper level on downwards, the old solution is stored into an container
     * object, before the grid is adapted. Father elements hold averaged information
     * from the son cells for the case of the sons being coarsened.
     *
     * @param problem The current problem
     */
    void storePrimVars(Problem& problem)
    {
        growthMap_.resize();

        // loop over all levels of the grid
        for (int level = grid_.maxLevel(); level >= 0; level--)
        {
            //get grid view on level grid
            LevelGridView levelView = grid_.levelGridView(level);

            if(!isBox)
            {
                for (ElementLevelIterator eIt = levelView.template begin<0>(); eIt != levelView.template end<0>(); ++eIt)
                {
                    //get your map entry
                    GrowthValues &growthVars = growthMap_[*eIt];

                    // put your value in the map
                    if (eIt->isLeaf())
                    {
                        // get index
                        int eIdx = this->elementIndex(problem, *eIt);
                        std::vector<double > spatialParams;
                        spatialParams.resize(8);
                        problem.spatialParams().getRootParams(*eIt, spatialParams);

                        storeGrowthValues(growthVars, problem.model().curSol()[eIdx], spatialParams);

                    }
                }
            }
            else
            {
                // Not implemented
            }
        }
    }

    /*!
     * Reconstruct missing primary variables (where elements are created/deleted)
     *
     * To reconstruct the solution in father elements, problem properties might
     * need to be accessed.
     * Starting from the lowest level, the old solution is mapped on the new grid:
     * Where coarsened, new cells get information from old father element.
     * Where refined, a new solution is reconstructed from the old father cell,
     * and then a new son is created. That is then stored into the general data
     * structure (CellData).
     *
     * @param problem The current problem
     */
    void reconstructPrimVars(Problem& problem)
    {
        growthMap_.resize();

        for (int level = 0; level <= grid_.maxLevel(); level++)
        {
            LevelGridView levelView = grid_.levelGridView(level);
            for (ElementLevelIterator eIt = levelView.template begin<0>(); eIt != levelView.template end<0>(); ++eIt)
            {
                // only treat non-ghosts, ghost data is communicated afterwards
                if (eIt->partitionType() == Dune::GhostEntity)
                    continue;

                if (!eIt->isNew())
                {
                    //entry is in map, write in leaf
                    if (eIt->isLeaf())
                    {
                        if(!isBox)
                        {
                            GrowthValues &growthVars = growthMap_[*eIt];
                            int newEIdx = this->elementIndex(problem, *eIt);
                            std::vector<double > spatialParams;
                            spatialParams.resize(8);
                            problem.spatialParams().getRootParams(*eIt, spatialParams);

                            setGrowthValues(growthVars, problem.model().curSol()[newEIdx], spatialParams );
                        }
                        else
                        {
                            // Not implemented
                        }
                    }
                }
                else
                {
                    if(!isBox)
                    {
                        // if we are on leaf, store reconstructed values of son in CellData object
                        if (eIt->isLeaf())
                        {
                            // acess new CellData object
                            int newEIdx = this->elementIndex(problem, *eIt);

                            PrimaryVariables priVars0 = PrimaryVariables(0.0);
                            std::vector<double > spatialParams0(0.0);
                            spatialParams0.resize(8);
                            //spatialParams = 0.0;
                            int count = 0;
                            for(auto&& intersection : intersections(gridView_, *eIt)) {
                                if (intersection.neighbor()){

                                    int neighborIndex = intersection.indexInInside();
                                    priVars0 += problem.model().prevSol()[neighborIndex];

                                    std::vector<double > spatialParams;
                                    spatialParams.resize(8);
                                    problem.spatialParams().getRootParams(*intersection.outside(), spatialParams);
                                    for (unsigned int i = 0; i < spatialParams0.size(); i++){
                                        if (i == 2  ) //order
                                            spatialParams0[i] += 2;
                                        else
                                            spatialParams0[i] += spatialParams[i];
                                    }
                                    count++;
                                }
                            }
                            priVars0  /= count;

                            for (unsigned int i = 0; i < spatialParams0.size(); i++)
                                spatialParams0[i] /= count;

                            Scalar time = problem.timeManager().time() +  problem.timeManager().timeStepSize();
                            spatialParams0[7] = time;
                            // set current solution for new element in solution vector
                            problem.model().curSol()[newEIdx] = priVars0;

                            // set new spatial params for new root element
                            problem.spatialParams().insertRootParams(*eIt,  spatialParams0);

                            int branchIdx = (int)spatialParams0[3];
                            problem.spatialParams().insertBranch(branchIdx,*eIt);

                        }
                    }
                    else
                    {
                        // Not implemented
                    }
                }
            }
        }
        // reset entries in restrictionmap
        growthMap_.resize( typename PersistentContainer::Value() );
        growthMap_.shrinkToFit();
        growthMap_.fill( typename PersistentContainer::Value() );
    }

    //! Stores values to be adapted in an adaptedValues container
    /**
     * Stores values to be adapted from the current CellData objects into
     * the adaptation container in order to be mapped on a new grid.
     *
     * \param adaptedValues Container for model-specific values to be adapted
     * \param element The element to be stored
     */
    static void storeGrowthValues(GrowthValues& growthVars, const PrimaryVariables& u, const std::vector<double > & spatialVars)
    {
        growthVars.u = u;
        growthVars.spatialVars = spatialVars;
    }

    //! Set adapted values in CellData
    /**
     * This methods stores reconstructed values into the cellData object, by
     * this setting a newly mapped solution to the storage container of the
     * decoupled models.
     *
     * \param adaptedValues Container for model-specific values to be adapted
     * \param element The element where things are stored.
     */
    static void setGrowthValues(GrowthValues& growthVars, PrimaryVariables& u, std::vector<double > & spatialVars)
    {
        u = growthVars.u;
        spatialVars =growthVars.spatialVars;
    }

    int elementIndex(const Problem& problem, const Element& element) const
    {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                return problem.elementMapper().index(element);
#else
                return problem.elementMapper().map(element);
#endif
    }

};
}
#endif
