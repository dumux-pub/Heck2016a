// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Class for a mapper between geometrical quantities of two grids,
 * a 1D and a 3D grid, and a container for physical quantities connected to
 * those information
 */

#ifndef DUMUX_1D3D_MAPPER_HH
#define DUMUX_1D3D_MAPPER_HH

#include <dune/grid/common/scsgmapper.hh>

namespace Dumux
{

namespace Properties
{
// forward declaration of properties
NEW_PROP_TAG(NumEq); //number of primary variables
NEW_PROP_TAG(Scalar); //scalar type
}

template <class DataContainer>
struct OneDThreeDMappedType
{
    int elementIdx;                 //!the index of the mapped element
    double fraction;                //!fraction of the element that intersects
    double length;                  //!length of the one-dimensional element
    double radius;                  //!radius of a tube shaped 1d element
    DataContainer data;             //!data container for physical data
};

template <class DataContainer>
class OneDThreeDMap : public std::multimap<int, OneDThreeDMappedType<DataContainer> > {};

template <class TypeTag, class Glue, class DataContainer1, class DataContainer2>
class OneDThreeDMapper
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef OneDThreeDMap<DataContainer1> MapType1;
    typedef OneDThreeDMap<DataContainer2> MapType2;

public:
    OneDThreeDMapper(const Glue &glue, MapType1 &oneDMap, MapType2 &threeDMap, bool verbose = false)
    {
        std::cout << "Writing the 3d (to 1d) map."<< std::endl;
        writeElementIntersectionMap<MapType2, DataContainer2, 0>(glue, threeDMap, verbose);
        std::cout << "Writing the 1d (to 3d) map."<< std::endl;
        writeElementIntersectionMap<MapType1, DataContainer1, 1>(glue, oneDMap, verbose);
    }

private:
    template <class MapType, class DataContainer, int side>
    static void writeElementIntersectionMap(const Glue& glue, MapType &mymm, bool verbose = false)
    {
        // Clear the map
        mymm.clear();

        // depending on the template parameter side, get the right GridView and Mapper
        // side=0 is the domain (2D/3D) and side=1 is the target (1D)
        typedef typename std::conditional<(side==0),
                                typename Glue::Grid0View,
                                typename Glue::Grid1View >::type GridView;

        typedef typename std::conditional<(side==0),
                                typename Glue::Grid1View,
                                typename Glue::Grid0View >::type GridViewOther;

        typedef typename std::conditional<(side==0),
                            typename Glue::Grid0IntersectionIterator,
                            typename Glue::Grid1IntersectionIterator>::type RemoteIntersectionIterator;

        typedef typename Dune::SingleCodimSingleGeomTypeMapper<GridView, 0> ::SingleCodimSingleGeomTypeMapper Mapper;
        typedef typename Dune::SingleCodimSingleGeomTypeMapper<GridViewOther, 0> ::SingleCodimSingleGeomTypeMapper MapperOther;

        std::multimap<int, RemoteIntersectionIterator> isMultiMap;

        // Element (to index) mapper
        Mapper mapper(glue.template patch<side>().gridView());
        MapperOther mapperOther(glue.template patch<1-side>().gridView());

        // setting up the map
        for (RemoteIntersectionIterator isIt = glue.template ibegin<side>();
            isIt != glue.template iend<side>();
            ++isIt)
        {
            int eIdx = mapper.index(isIt->inside());
               int eIdx2 = mapperOther.index(isIt->outside());
            OneDThreeDMappedType<DataContainer> mappedContainer;

            // set the element and intersection index that is mapped to
            mappedContainer.elementIdx = eIdx2;

            isMultiMap.insert(std::pair<int, RemoteIntersectionIterator>(eIdx, isIt));
            mymm.insert(std::pair<int, OneDThreeDMappedType<DataContainer> >(eIdx, mappedContainer));
        }

        // calculating the 1d fractions used below for the 3d map
        int gridsize1d = glue.template patch<1-side>().gridView().size(0);
        std::vector<Scalar> frac1d(gridsize1d, 0.0);
        RemoteIntersectionIterator isIt = glue.template ibegin<side>();
        RemoteIntersectionIterator isItEnd = glue.template iend<side>();
        if(side == 0) //calculate the 1d fractions for 3d map
        {
            for (; isIt != isItEnd; ++isIt)
            {
                int eIdxOther = mapperOther.index(isIt->outside());
                frac1d[eIdxOther] += isIt->geometry().volume()/ isIt->outside().geometry().volume();
                if(verbose) std::cout << "frac1d["<<eIdxOther<<"] = " << frac1d[eIdxOther] << std::endl;
            }
        }

        // calculating and inserting the fractions
        auto eIt =  glue.template patch<side>().gridView().template begin<0>();
        const auto eEndIt = glue.template patch<side>().gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdx = mapper.index(*eIt);
            Scalar fracSum = 0;
            std::vector<Scalar > frac(mymm.count(eIdx));

            if (mymm.count(eIdx) != 0)
            {
                Scalar isVol = 0.0;
                Scalar isVolSum = 0.0;
                Scalar eVol = 0.0;
                int k = 0;

                if(side == 1) //1d->3d map
                {
                    eVol = eIt->geometry().volume(); //1d map
                    //1d
                    for (auto isIt=isMultiMap.equal_range(eIdx).first; isIt!=isMultiMap.equal_range(eIdx).second; ++isIt)
                    {
                        isVol = (*isIt).second->geometry().volume();
                        isVolSum += (*isIt).second->geometry().volume();
                        frac[k] = isVol/eVol;
                        k++;
                    }

                    auto mymmIter = mymm.equal_range(eIdx).first;
                    for (int m = 0; m < frac.size(); m++)
                    {
                        frac[m] = frac[m]/(isVolSum/eVol);
                        fracSum +=  frac[m];
                        (*mymmIter).second.fraction =  frac[m];
                        ++mymmIter;
                    }
                    if(verbose) std::cout << "fraction sum: " << fracSum <<  std::endl;
                }
                else if (side == 0) //3d->1d map
                {
                    //3d
                    auto mymmIter = mymm.equal_range(eIdx).first;
                    for (auto isIt=isMultiMap.equal_range(eIdx).first; isIt!=isMultiMap.equal_range(eIdx).second; ++isIt)
                    {
                        isVol = (*isIt).second->geometry().volume();
                        eVol = (*isIt).second->outside().geometry().volume();

                        (*mymmIter).second.fraction = isVol/eVol/frac1d[(*mymmIter).second.elementIdx];
                        (*mymmIter).second.length = isVol;
                        k++;
                        ++mymmIter;
                    }
                }
            }
        }

        // test multimaps and print output for debug purposes
        if(verbose) {
            auto eIt =  glue.template patch<side>().gridView().template begin<0>();
            const auto eEndIt = glue.template patch<side>().gridView().template end<0>();

            for (; eIt != eEndIt; ++eIt)
            {
                int eIdx = mapper.index(*eIt);
                std::cout << "There are " << mymm.count(eIdx) << " elements with key " << eIdx << ":"<< std::endl;

                for (auto mymmIter=mymm.equal_range(eIdx).first; mymmIter!=mymm.equal_range(eIdx).second; ++mymmIter)
                {
                    std::cout << "elementIdx: " << (*mymmIter).second.elementIdx
                              << " fraction: " << (*mymmIter).second.fraction << std::endl;
                }
            }
        }
    }
};



} //namespace Dumux

#endif