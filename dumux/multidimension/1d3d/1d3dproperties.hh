// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for problems which involve two sub problems, where one
 * problem is a one-dimensional problem embedded in a second three-dimensional problem
 * coupled through source/sink terms
 */

#ifndef DUMUX_1D3D_PROPERTIES_HH
#define DUMUX_1D3D_PROPERTIES_HH

#include <dumux/multidimension/common/multidimensionproblem.hh>
#include "1d3dmapper.hh"

namespace Dumux
{

namespace Properties
{
NEW_TYPE_TAG(OneDThreeDModel, INHERITS_FROM(MultiDimensionProblem));

// property forward declarations
NEW_PROP_TAG(DataContainer1);
NEW_PROP_TAG(DataContainer2);

SET_PROP(OneDThreeDModel, MultiDimensionMapper)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, DataContainer1) DataContainer1;
    typedef typename GET_PROP_TYPE(TypeTag, DataContainer2) DataContainer2;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;
    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridView) GridView2;
    typedef Dune::GridGlue::Codim0Extractor<GridView1> TarExtractor;
    typedef Dune::GridGlue::Codim0Extractor<GridView2> DomExtractor;
    typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;
public:
    typedef OneDThreeDMapper<TypeTag, GlueType, DataContainer1, DataContainer2> type;
};

}//end namespace Properties

}//end namespace Dumux

#endif