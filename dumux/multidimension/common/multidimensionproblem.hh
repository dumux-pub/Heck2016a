// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for multidimensional problems which involve
 * two sub problems coupled through source/sink terms
 */

#ifndef DUMUX_MULTIDIMENSION_PROBLEM_HH
#define DUMUX_MULTIDIMENSION_PROBLEM_HH

#include <dumux/common/basicproperties.hh>
#include <dumux/common/timemanager.hh>
#include <dumux/io/gridgluevtkwriter.hh>

#include <dune/istl/bvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>

// dune grid-glue includes
#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/grid-glue/gridglue.hh>
#include <dune/grid-glue/merging/overlappingmerge.hh>

namespace Dumux
{

namespace Properties
{
// NumericModel provides Scalar, GridCreator, ParameterTree
NEW_TYPE_TAG(MultiDimensionProblem, INHERITS_FROM(NumericModel));

// property forward declarations
NEW_PROP_TAG(CouplingTransferData);
NEW_PROP_TAG(GridView1);
NEW_PROP_TAG(GridView2);

// property tags that will be set in the problem at hand
NEW_PROP_TAG(SubProblem1TypeTag);
NEW_PROP_TAG(SubProblem2TypeTag);
NEW_PROP_TAG(Problem);
NEW_PROP_TAG(MultiDimensionMapper);

// property tags with default value
NEW_PROP_TAG(TimeManager);
NEW_PROP_TAG(Grid);
NEW_PROP_TAG(SolutionVector);

// default property value for the time manager
SET_TYPE_PROP(MultiDimensionProblem, TimeManager, Dumux::TimeManager<TypeTag>);

// default property value for the grid1
SET_PROP(MultiDimensionProblem, Grid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
public:
    typedef typename GET_PROP_TYPE(SubTypeTag1, Grid) type;
};

// default property value for the solution vector
SET_PROP(MultiDimensionProblem, SolutionVector)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
public:
    typedef typename GET_PROP_TYPE(SubTypeTag1, SolutionVector) type;
};
}//end namespace Properties

// // TODO Doc me!
// /** \brief Returns always true */
// template <class GridView>
// class AllElementsDescriptor
//     : public Dune::GridGlue::ExtractorPredicate<GridView, 0>
// {
// public:
//     virtual bool contains(const typename GridView::Traits::template Codim<0>::EntityPointer& element, unsigned int subentity) const
//     {
//         return true;
//     }
// };

/*!
 * \ingroup ModelCoupling
 * \brief Base class for multidimensional problems which involve two sub problems
 */
template<class TypeTag>
class MultiDimensionProblem
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDimensionMapper) MultiDimensionMapper;

    // obtain the type tags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;

    // obtain all other types from the SubTypeTags
    typedef typename GET_PROP_TYPE(SubTypeTag1, Problem) SubProblem1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Problem) SubProblem2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, TimeManager) SubTimeManager1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, TimeManager) SubTimeManager2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridView) GridView2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, CouplingTransferData) MapType1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, CouplingTransferData) MapType2;

    typedef Dune::GridGlue::Codim0Extractor<GridView1> TarExtractor;
    typedef Dune::GridGlue::Codim0Extractor<GridView2> DomExtractor;
    typedef Dune::GridGlue::GridGlue<DomExtractor,TarExtractor> GlueType;

    enum {
            dim1 = GridView1::dimension,
            dim2 = GridView2::dimension,
            dimworld1 = GridView1::dimensionworld,
            dimworld2 = GridView2::dimensionworld
         };

     typedef typename GridView2::template Codim<0>::Entity BulkElement;
    typedef typename GridView1::template Codim<0>::Entity LowDimElement;
public:
    MultiDimensionProblem(TimeManager &timeManager, const GridView1 &gridView1, const GridView2 &gridView2)
    : timeManager_(timeManager), gridView1_(gridView1), gridView2_(gridView2)
    {
        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeTime);
        this->timeManager().startNextEpisode(episodeTime);
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem and the sub-problems.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        // set start time for the sub problems
        Scalar tStart = timeManager().time();

        // set end time for the sub problems
        Scalar tEnd = tStart + timeManager().timeStepSize();

        bool restart = false;
        // HACK: assume that we restart if time > 0
        if (tStart > 0)
            restart = true;

        timeManager().startNextEpisode(episodeTime);
        asImp_().subTimeManager1().startNextEpisode(episodeTime);
        asImp_().subTimeManager2().startNextEpisode(episodeTime);

        // get initial time step size for the subproblems
        Scalar dtSubProblem1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitialSubProblem1);
        Scalar dtSubProblem2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitialSubProblem2);

        // initialize the subproblem time managers (this also initializes the subproblems)
        asImp_().subTimeManager1().init(asImp_().subProblem1(), tStart, dtSubProblem1, tEnd, restart);
        asImp_().subTimeManager2().init(asImp_().subProblem2(), tStart, dtSubProblem2, tEnd, restart);

        // glue the two grids (find intersections)
        callGridGlue();
    }

    /*!
     * \brief This method writes the complete state of the simulation
     *        to the harddisk.
     *
     * The file will start with the prefix returned by the name()
     * method, has the current time of the simulation clock in it's
     * name and uses the extension <tt>.drs</tt>. (Dumux ReStart
     * file.)  See Dumux::Restart for details.
     */
    void serialize()
    {}

    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {}

    /*!
     * \brief Called by Dumux::TimeManager in order to do a time
     *        integration on the model. Algorithms for the time integration are
     *        implemented here. The default algorithm calls the subproblems without
     *        exchanging data (completely decoupled). Overwrite this method in the
     *        implementation.
     */
    void timeIntegration()
    {
        std::cout << std::endl;
        std::cout << "coupled time integration at t = " << this->timeManager().time() << std::endl;

        // run first model, the 1D model
        asImp_().subTimeManager1().setTime(this->timeManager().time());
        asImp_().subTimeManager1().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        asImp_().subTimeManager1().setTimeStepSize(asImp_().subTimeManager1().previousTimeStepSize());
        std::cout << "SOLVE 1D MODEL " << std::endl;
        asImp_().subTimeManager1().run();

        // run second model, the 3d model
        asImp_().subTimeManager2().setTime(this->timeManager().time());
        asImp_().subTimeManager2().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        asImp_().subTimeManager2().setTimeStepSize(asImp_().subTimeManager2().previousTimeStepSize());
        std::cout << "SOLVE 3D MODEL " << std::endl;
        asImp_().subTimeManager2().run();
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {}

    /*!
     * \brief Called by Dumux::TimeManager whenever a solution for a
     *        timestep has been computed and the simulation time has
     *        been updated.
     */
    Scalar nextTimeStepSize(const Scalar dt)
    {
        return dt;
    }

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     */
    bool shouldWriteOutput() const
    {
        return
            (timeManager().episodeWillBeOver() ||
             timeManager().willBeFinished()) ;
    }

    /*!
     * \brief Returns true if the current state of the simulation
     * should be written to disk
     */
    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    {
        timeManager().startNextEpisode(episodeTime);
        timeManager().setTimeStepSize(timeManager_.timeStepSize());
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     * It could be either overwritten by the problem files, or simply
     * declared over the setName() function in the application file.
     */
    const char *name() const
    {
        return "multidimensionproblem";
    }

    /*!
     * \brief Called by the time manager after everything which can be
     *        done about the current time step is finished and the
     *        model should be prepared to do the next time integration.
     */
    void advanceTimeLevel()
    {
        asImp_().subProblem1().advanceTimeLevel();
        asImp_().subProblem2().advanceTimeLevel();
    }

    /*!
     * \brief Write the relevant quantities of the current solution into
     * an VTK output file.
     */
    void writeOutput()
    {
        // write the current result to disk
        if (asImp_().shouldWriteOutput()) {
            asImp_().subProblem1().writeOutput();
            asImp_().subProblem2().writeOutput();
        }
    }

    /*!
     * \brief Load a previously saved state of the whole simulation
     *        from disk.
     *
     * \param tRestart The simulation time on which the program was
     *                 written to disk.
     */
    void restart(const Scalar tRestart)
    {}

    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    //! Returns the time manager of the multidimension problem
    const TimeManager &timeManager() const
    { return timeManager_; }

    //! \copydoc timeManager()
    TimeManager &timeManager()
    { return timeManager_; }

    //! A return function that needs to be implemented in the implementation of this problem
    SubProblem1 &subProblem1()
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subProblem1 method!"); }
    //! \copydoc subProblem1()
    const SubProblem1 &subProblem1() const
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subProblem1 method!"); }
    //! \copydoc subProblem1()
    SubProblem2 &subProblem2()
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subProblem2 method!"); }
    //! \copydoc subProblem1()
    const SubProblem2 &subProblem2() const
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subProblem2 method!"); }
    //! \copydoc subProblem1()
    const SubTimeManager1 &subTimeManager1() const
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subTimeManager1 method!"); }
    //! \copydoc subProblem1()
    SubTimeManager1 &subTimeManager1()
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subTimeManager1 method!"); }
    //! \copydoc subProblem1()
    const SubTimeManager2 &subTimeManager2() const
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subTimeManager2 method!"); }
    //! \copydoc subProblem1()
    SubTimeManager2 &subTimeManager2()
    { DUNE_THROW(Dune::NotImplemented, "Multidimension problems need to implement the subTimeManager2 method!"); }

    /*!
     * \brief Returns a reference to the map of subproblem1 data to subproblem2 data
     */
    MapType1 &map1()
    { return map1_; }

    /*!
     * \brief Returns a reference to the map of subproblem2 data to subproblem1 data
     */
    MapType2 &map2()
    { return map2_; }

    TimeManager &timeManager_;

    GridView1 gridView1_;
    GridView2 gridView2_;

    MapType1 map1_;
    MapType2 map2_;

    Scalar episodeTime;

    /*!
     * \brief Build the grid glue object that glues the two grids together returning
     * intersection information. This information can be plotted to vtk files.
     * Also write the maps for data transfer between the subproblems and fill it with
     * geometrical data. The physical data is the responsibility of the implementation
     * of this problem.
     */
    void callGridGlue()
    {
        // target: gridview1
        // domain: gridview2

        if(dimworld1 != dimworld2)
            DUNE_THROW(Dune::InvalidStateException, "Cannot glue grids with different world dimensions");

        // Initialize the descriptors (all elements)
        auto DomDescriptor = [](const BulkElement& element, unsigned int subentity) { return true; };
        auto TarDescriptor = [](const LowDimElement& element, unsigned int subentity) { return true; };

        TarExtractor tarEx(gridView1_, TarDescriptor);
        DomExtractor domEx(gridView2_, DomDescriptor);

        Dune::GridGlue::OverlappingMerge<dim2, dim1, dimworld1> merger;
        GlueType glue(domEx, tarEx, &merger);

        //calculate the intersections
        glue.build();

        //write intersections to file for debug purposes
        GridGlueVtkWriter glueVtk;
        glueVtk.write(glue, name());

        //write maps of geometric information
        MultiDimensionMapper mapper(glue, map1_, map2_);
    }
};

}//end namespace Dumux

#endif