// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for problems which involve two sub problems
 */
#ifndef DUMUX_COUPLED_RICHARDS_ROOTSYSTEM_HH
#define DUMUX_COUPLED_RICHARDS_ROOTSYSTEM_HH

#include <dumux/common/basicproperties.hh>
#include <dumux/common/timemanager.hh>


//#include <config.h>
#include <dune/istl/bvector.hh>
#include <dune/grid/common/scsgmapper.hh>

//#include <dune/common/parallel/mpihelper.hh>
//#include <dune/grid/utility/structuredgridfactory.hh>
//#include <dune/grid/sgrid.hh>
//#include <dune/grid/geometrygrid.hh>

// INCLUDES FOR GRID-GLUE
#include <dune/grid-glue/extractors/extractorpredicate.hh>
#include <dune/grid-glue/extractors/codim0extractor.hh>
#include <dune/grid-glue/adapter/gridglue.hh>
#include <dune/grid-glue/merging/mixeddimoverlappingmerge.hh>

#include <dune/grid-glue/test/couplingtest.hh>
#include <dune/grid-glue/adapter/gridgluevtkwriterroot.hh>

using namespace Dune::GridGlue;
namespace Dumux
{

namespace Properties
{
// new type tag for the simple coupling
// NumericModel provides Scalar, GridCreator, ParameterTree
NEW_TYPE_TAG(CoupledRichardsRootSystem, INHERITS_FROM(NumericModel));

// property forward declarations
NEW_PROP_TAG(GridView1);
NEW_PROP_TAG(GridView2);

// property tags that will be set in the problem at hand
NEW_PROP_TAG(SubProblem1TypeTag);
NEW_PROP_TAG(SubProblem2TypeTag);
NEW_PROP_TAG(Problem);

// property tags with default value
NEW_PROP_TAG(TimeManager);
NEW_PROP_TAG(Grid);
NEW_PROP_TAG(SolutionVector);

// default property value for the time manager
SET_TYPE_PROP(CoupledRichardsRootSystem, TimeManager, Dumux::TimeManager<TypeTag>);

// default property value for the grid1
SET_PROP(CoupledRichardsRootSystem, Grid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
public:
    typedef typename GET_PROP_TYPE(SubTypeTag1, Grid) type;
};

// default property value for the solution vector
SET_PROP(CoupledRichardsRootSystem, SolutionVector)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
public:
    typedef typename GET_PROP_TYPE(SubTypeTag1, SolutionVector) type;
};

}

/** \brief Returns always true */
template <class GridView>
class AllElementsDescriptor
    : public ExtractorPredicate<GridView,0>
{
public:
    virtual bool contains(const typename GridView::Traits::template Codim<0>::EntityPointer& element, unsigned int subentity) const
    {
        return true;
    }
};
    /*!
 * \ingroup ModelCoupling
 * \brief Base class for problems which involve two sub problems
 *
 * \todo Please doc me more!
 */
template<class TypeTag>
class CoupledRichardsRootSystemProblem
{
private:
    // the following properties are also required by start.hh, so they are
    // contained in the coupled TypeTag
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // obtain the type tags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;

    // obtain all other types from the SubTypeTags
    typedef typename GET_PROP_TYPE(SubTypeTag1, Problem) SubProblem1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Problem) SubProblem2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, TimeManager) SubTimeManager1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, TimeManager) SubTimeManager2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridView) GridView2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, SolutionVector) SolutionVectorRoot;
    typedef typename GET_PROP_TYPE(SubTypeTag2, SolutionVector) SolutionVectorMatrix;

    //typedef typename GET_PROP_TYPE(SubTypeTag1, ElementVolumeVariables) ElementVolumeVariablesRoot;
    //typedef typename GET_PROP_TYPE(SubTypeTag1, ElementSolutionVector) ElementSolutionVectorRoot;

    //typedef typename GridView1 DomGridView;
    //typedef typename GridView2 TarGridView;
    typedef Dune::GridGlue::Codim0Extractor<GridView1> TarExtractor;
    typedef Dune::GridGlue::Codim0Extractor<GridView2> DomExtractor;
    typedef ::GridGlue<DomExtractor,TarExtractor> GlueType;

public:
    CoupledRichardsRootSystemProblem(TimeManager &timeManager, const GridView1 &gridView1, const GridView2 &gridView2)
    : timeManager_(timeManager),
    subProblem1_(subTimeManager1_, gridView1), subProblem2_(subTimeManager2_, gridView2),
                                   gridView1_(gridView1),gridView2_(gridView2)
    {

        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                   Scalar,
                                                   TimeManager,
                                                   EpisodeTime);
        timeManager_.startNextEpisode(episodeTime);


    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem and the sub-problems.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        // set start time for the sub problems
        Scalar tStart = timeManager_.time();

        // set end time for the sub problems
        Scalar tEnd = tStart + timeManager_.timeStepSize();

        bool restart = false;
        // HACK: assume that we restart if time > 0
        if (tStart > 0)
            restart = true;

        timeManager_.startNextEpisode(episodeTime);
        subTimeManager1_.startNextEpisode(episodeTime);
        subTimeManager2_.startNextEpisode(episodeTime);

        // get initial time step size for the subproblems
        Scalar dtSubProblem1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitialSubProblem1);
        Scalar dtSubProblem2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitialSubProblem2);

        callGridGlue();

        subProblem1_.setMap(&mymm1_);
        subProblem2_.setMap(&mymm0_);

        // initialize the subproblem time managers
        // (this also initializes the subproblems)
        subTimeManager1_.init(subProblem1_, tStart, dtSubProblem1, tEnd, restart);
        subTimeManager2_.init(subProblem2_, tStart, dtSubProblem2, tEnd, restart);
    }

    /*!
     * \brief This method writes the complete state of the simulation
     *        to the harddisk.
     *
     * The file will start with the prefix returned by the name()
     * method, has the current time of the simulation clock in it's
     * name and uses the extension <tt>.drs</tt>. (Dumux ReStart
     * file.)  See Dumux::Restart for details.
     */
    void serialize()
    {}

    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {
        updateMapRoot();
        updateMapSoil();

        preSolRoot = subProblem1_.model().curSol();
        preSolSoil = subProblem2_.model().curSol();

    }

    /*!
     * \brief Called by Dumux::TimeManager in order to do a time
     *        integration on the model.
     */
    void timeIntegration()
    {
        std::cout << "coupled timeIntegration t = " << timeManager_.time() << std::endl;
        bool reIterate = true;

        SolutionVectorRoot sourceRoot;
        SolutionVectorRoot sourceRoot2;
        SolutionVectorRoot diffRoot;
        Scalar normRoot;

        SolutionVectorMatrix sourceSoil;
        SolutionVectorMatrix sourceSoil2;
        SolutionVectorMatrix diffSoil;
        Scalar normSoil;

        Scalar eps = 1e-10;
        int itNum = 0;

        // update root source
        updateMapRoot();
        // run first model -> Root
        subTimeManager1_.setTime(timeManager_.time());
        subTimeManager1_.setEndTime(timeManager_.time() + timeManager_.timeStepSize());
        subTimeManager1_.setTimeStepSize(subTimeManager1_.previousTimeStepSize());
        std::cout << "SOLVE ROOT " << std::endl;
        subTimeManager1_.run();
        // get root source values
        subProblem1_.model().sourceValues(sourceRoot);

        // update soil source
        updateMapSoil();
        // run second model -> Soil
        subTimeManager2_.setTime(timeManager_.time());
        subTimeManager2_.setEndTime(timeManager_.time() + timeManager_.timeStepSize());
        subTimeManager2_.setTimeStepSize(subTimeManager2_.previousTimeStepSize());
        std::cout << "SOLVE SOIL " << std::endl;
        subTimeManager2_.run();
        // get soil source values
        subProblem2_.model().sourceValues(sourceSoil);

        while (reIterate)
            {
                normRoot = 0.0;
                normSoil = 0.0;

                //update and re-run root model
                updateMapRoot();
                std::cout << "SOLVE ROOT " << std::endl;
                subTimeManager1_.run();

                // get root source values
                subProblem1_.model().sourceValues(sourceRoot2);

                // calc norm of root sink between the last iteration steps
                diffRoot.resize(sourceRoot2.size());
                for (int j = 0; j < int(sourceRoot2.size()); ++j) {
                    diffRoot[j][0] = (sourceRoot[j][0] - sourceRoot[j][0]);
                    normRoot += std::abs(diffRoot[j][0]) * std::abs(diffRoot[j][0]);
                }
                if (normRoot!= 0)
                     normRoot = std::sqrt(normRoot);
                std::cout <<"Norm Root: " << normRoot  << std::endl;

                //update and re-run soil model
                updateMapSoil();
                std::cout << "SOLVE SOIL " << std::endl;
                subTimeManager2_.run();

                // get soil source values
                subProblem2_.model().sourceValues(sourceSoil2);

                // calc norm of soil sink between the last iteration steps
                diffSoil.resize(sourceSoil2.size());
                for (int j = 0; j < int(sourceSoil2.size()); ++j) {
                    if (sourceSoil[j][0] != 0 &&  sourceSoil2[j][0] != 0) {
                        diffSoil[j][0] = (sourceSoil[j][0] - sourceSoil2[j][0]);
                        normSoil +=  std::abs(diffSoil[j][0]) *  std::abs(diffSoil[j][0]);
                    }
                }
                if (normSoil!= 0)
                    normSoil = std::sqrt(normSoil);
                std::cout <<"Norm Soil: " << normSoil  << std::endl;

                // if both norms are smaller eps continue with next time step
                if (normSoil < eps && normRoot < eps)
                    reIterate = false;
                // if not
                else
                    {
                        //update source terms
                        sourceSoil = sourceSoil2;
                        sourceRoot = sourceRoot2;
                        //count interation
                        itNum ++;
                        std::cout <<"Reiterate Models: " << itNum  << std::endl;
                    }
            }
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
    }

    /*!
     * \brief Called by Dumux::TimeManager whenever a solution for a
     *        timestep has been computed and the simulation time has
     *        been updated.
     */
    Scalar nextTimeStepSize(const Scalar dt)
    {
        return dt;
    }

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     */
    bool shouldWriteOutput() const
    {
        return
            (timeManager_.episodeWillBeOver() ||
             timeManager_.willBeFinished()) ;
    }

    /*!
     * \brief Returns true if the current state of the simulation
     * should be written to disk
     */
    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    {
        timeManager_.startNextEpisode(episodeTime);
        timeManager_.setTimeStepSize(timeManager_.timeStepSize());
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     * It could be either overwritten by the problem files, or simply
     * declared over the setName() function in the application file.
     */
    const char *name() const
    {
        return "coupledrichardsrootsystem";
    }

    /*!
     * \brief Called by the time manager after everything which can be
     *        done about the current time step is finished and the
     *        model should be prepared to do the next time integration.
     */
    void advanceTimeLevel()
    {
        subProblem1_.advanceTimeLevel();
        subProblem2_.advanceTimeLevel();
    }

    /*!
     * \brief Write the relevant quantities of the current solution into
     * an VTK output file.
     */
    void writeOutput()
    {
        // write the current result to disk
        if (asImp_().shouldWriteOutput()) {
            subProblem1_.writeOutput();
            subProblem2_.writeOutput();
        }
    }

    /*!
     * \brief Load a previously saved state of the whole simulation
     *        from disk.
     *
     * \param tRestart The simulation time on which the program was
     *                 written to disk.
     */
    void restart(const Scalar tRestart)
    {}

protected:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    /*!
     * \brief Returns a reference to subproblem1
     */
    SubProblem1& subProblem1()
    { return subProblem1_; }

    /*!
     * \brief Returns a const reference to subproblem1
     */
    const SubProblem1& subProblem1() const
    { return subProblem1_; }

    /*!
     * \brief Returns a reference to subproblem2
     */
    SubProblem2& subProblem2()
    { return subProblem2_; }

    /*!
     * \brief Returns a const reference to subproblem2
     */
    const SubProblem2& subProblem2() const
    { return subProblem2_; }


    TimeManager &timeManager_;
    SubTimeManager1 subTimeManager1_;
    SubTimeManager2 subTimeManager2_;


private:
    SubProblem1 subProblem1_;
    SubProblem2 subProblem2_;

    const GridView1 gridView1_;
    const GridView2 gridView2_;

    std::multimap<int,data2test > mymm0_;
    std::multimap<int,data2test > mymm1_;

    SolutionVectorRoot preSolRoot;
    SolutionVectorMatrix preSolSoil;

    Scalar episodeTime;

   // doc me!!!
    void callGridGlue()
    {
        // target: gridview1 (1D)
        // domain: gridview2 (2D/3D)

        AllElementsDescriptor<GridView1> tardesc;
        AllElementsDescriptor<GridView2> domdesc;

        DomExtractor domEx(gridView2_, domdesc);
        TarExtractor tarEx(gridView1_, tardesc);

        MixedDimOverlappingMerge<3,1,3> merger;
        GlueType glue(domEx, tarEx, &merger);

        glue.build();

        GridGlueVtkWriterRoot glueVtk;
        glueVtk.writeMaps(glue, mymm0_, mymm1_);

        glueVtk.write(glue,"/temp/Natalie/DUMUX/dumux-devel/appl/rosi/richardsrootsystemDGF/");
    }

    // update the Root- multimap (used by subProblem1 = RootSystemProblem)
    void updateMapRoot()
    {
        // get soil pressure values from the current solution
        const SolutionVectorMatrix& solSoil = subProblem2_.model().curSol();

        //write pressure values into multimap
        typename std::multimap<int,data2test >::iterator it1;
        for (it1=mymm1_.begin(); it1 != mymm1_.end(); ++it1)
            {
                (*it1).second.data = solSoil[(*it1).second.elm];
            }

        subProblem1_.setMap(&mymm1_);
    }

    // update the Soil- multimap (used by subProblem2 = RichardsProblem)
    void updateMapSoil()
    {
        //get source values from 1D RootProbelm
        SolutionVectorRoot sourceRoot;
        subProblem1_.model().sourceValues(sourceRoot);

        //write source values into multimap
        typename std::multimap<int,data2test>::iterator it;
        for (it=mymm0_.begin(); it != mymm0_.end(); ++it)
            {
                (*it).second.data = sourceRoot[(*it).second.elm];
            }
        subProblem2_.setMap(&mymm0_);
    }

};

}

#endif
