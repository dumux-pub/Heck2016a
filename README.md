Readme
===============================

Content
--------------
The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

- appl/rosi/katharina
- appl/rosi/katharina/2p2cni

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

- appl/rosi/katharina/test_coupledrosi.cc
- appl/rosi/katharina/2p2cni/test_rosi2p2c_homogeneous.cc
- appl/rosi/katharina/test_rosi_heterogeneous.cc
- appl/rosi/katharina/test_rosi_homogeneous.cc
- appl/rosi/katharina/2p2cni/test_rosi2p2c_homogeneous.cc

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

Installation
============

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above. For the basic
dependencies see dune-project.org.

The easiest way is to use the `installHeck2016a.sh` in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2016a/raw/master/installHeck2016a.sh). Create a new folder containing the script and execute it.
You can copy the following to a terminal:
```bash
mkdir -p Heck2016a && cd Heck2016a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Heck2016a/raw/master/installHeck2016a.sh
chmod +x installHeck2016a.sh && ./installHeck2016a.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.


Application
--------------------

For test on root water uptake alone, go to the folder appl/rosi/katharina.
test_rosi_homogeneous and test_rosi_heterogeneous offer test of one root in a soil cube. Richards equation is used as the transport
equation in the soil.
In appl/rosi/katharina/2p2cni a non-isothermal 2p2c model is used in the soil with water and air as the two
components. Evaporation is included as a boundary condition at the top of the soil. Execute test_rosi2p2c_homogeneous
for that test.

Used Version
--------------------
When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

- dune-common 2.4.1
- dune-geometry 2.4.1
- dune-grid 2.4.1
- dune-istl 2.4.1
- dune-localfuntions 2.4.1
- dune-foamgrid 2.4
- dune-grid-glue 2.4
- dumux-stable 2.9

Use the script installHeck2016a.sh to install these modules and execute dunecontrol.

## Installation with Docker

Create a new folder and change into it

```bash
mkdir Heck2016a
cd Heck2016a
```

Then download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Heck2016a/-/raw/master/docker_heck2016a.sh
```

and open the docker container by running
```bash
bash docker_heck2016a.sh open
```

After the script has run successfully, executable can be built
```bash
cd Heck2016a/build-cmake/appl/rosi/katharina
make test_rosi_heterogeneous
make test_rosi_homogeneous
cd 2p2cni
make test_rosi2p2c_homogeneous
```

One may run the executables, e.g.,
```bash
./test_rosi2p2c_homogeneous
```

