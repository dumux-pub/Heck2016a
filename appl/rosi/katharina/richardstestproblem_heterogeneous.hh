// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_RICHARDS_TEST_PROBLEM_HH
#define DUMUX_RICHARDS_TEST_PROBLEM_HH

#include <cmath>

//#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/porousmediumflow/richards/implicit/model.hh>

//#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

//! get the properties needed for subproblems
#include <dumux/modelcoupling/common/subproblemproperties.hh>

#include "richardstestspatialparams_heterogeneous.hh"
#include <dune/grid/yaspgrid.hh>
//#include <dune/alugrid/grid.hh>
//#include <dune/grid/io/file/dgfparser/dgfalu.hh>



namespace Dumux
{
template <class TypeTag>
class RichardTestProblemHeterogeneous;

namespace Properties
{
NEW_TYPE_TAG(RichardTestProblemHeterogeneous, INHERITS_FROM(Richards, RichardsTestSpatialParamsDGF));
NEW_TYPE_TAG(RichardsTestBoxProblem, INHERITS_FROM(BoxModel, RichardTestProblemHeterogeneous));
NEW_TYPE_TAG(RichardsTestCCProblem, INHERITS_FROM(CCModel, RichardTestProblemHeterogeneous));

// Set the wetting phase
SET_PROP(RichardTestProblemHeterogeneous, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};

// Set the grid type
// SET_PROP(RichardsTestProblem, Grid)
// {
//     typedef Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> > type;
// };

// Set the grid type
SET_TYPE_PROP(RichardTestProblemHeterogeneous, Grid, Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> >);
//SET_TYPE_PROP(RichardsTestProblem, Grid, Dune::UGGrid<3>);
//SET_TYPE_PROP(RichardTestProblemHeterogeneous, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_PROP(RichardTestProblemHeterogeneous, Problem)
{ typedef Dumux::RichardTestProblemHeterogeneous<TypeTag> type; };

// Set the spatial parameters
SET_PROP(RichardTestProblemHeterogeneous, SpatialParams)
{ typedef Dumux::RichardsTestSpatialParamsDGF<TypeTag> type; };

// Enable gravity
SET_BOOL_PROP(RichardTestProblemHeterogeneous, ProblemEnableGravity, true);

// Enable partial reassembly of the Jacobian matrix
SET_BOOL_PROP(RichardTestProblemHeterogeneous, ImplicitEnablePartialReassemble, true);

// Enable re-use of the Jacobian matrix for the first iteration of a time step
SET_BOOL_PROP(RichardTestProblemHeterogeneous, ImplicitEnableJacobianRecycling, true);

// Use forward differences to approximate the Jacobian matrix
SET_INT_PROP(RichardTestProblemHeterogeneous, ImplicitNumericDifferenceMethod, +1);

// Set the maximum number of newton iterations of a time step
SET_INT_PROP(RichardTestProblemHeterogeneous, NewtonMaxSteps, 28);

// Set the "desireable" number of newton iterations of a time step
SET_INT_PROP(RichardTestProblemHeterogeneous, NewtonTargetSteps, 18);

// write newton convergence to vtk
SET_BOOL_PROP(RichardTestProblemHeterogeneous, NewtonWriteConvergence, false);

// Enable velocity output
SET_BOOL_PROP(RichardTestProblemHeterogeneous, VtkAddVelocity, true);

SET_STRING_PROP(RichardTestProblemHeterogeneous, GridParameterGroup, "SoilGrid");

}

/*!
 * \ingroup OnePBoxModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::SGrid<2,2> type;</tt> to
 * <tt>typedef Dune::SGrid<3,3> type;</tt> in the problem file
 * and use <tt>1p_3d.dgf</tt> in the parameter file.
 */
template <class TypeTag>
class RichardTestProblemHeterogeneous : public RichardsProblem<TypeTag>
{
    typedef RichardsProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
     typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        contiEqIdx = Indices::contiEqIdx,
        hIdx = Indices::hIdx,
        pwIdx= Indices::pwIdx,
        //pressureIdx = Indices::pressureIdx
    };

     static const bool useHead = GET_PROP_VALUE(TypeTag, UseHead); // set in coupled problem file

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, CouplingTransferData) MapType;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
     typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    RichardTestProblemHeterogeneous(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        std::string coupledName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        pnRef_ = 1e5;
        eps_ = 1e-6;
        name_ = coupledName + "_SoilProblem3d";

        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                   Scalar,
                                                   TimeManager,
                                                   EpisodeTime);
        this->timeManager().startNextEpisode(episodeTime);// time episode
        lensLowerLeft_[0] = -0.01;
        lensLowerLeft_[1] = -0.01;
        lensLowerLeft_[2] = -0.025;

        lensUpperRight_[0] = 0.01;
        lensUpperRight_[1] = 0.01;
        lensUpperRight_[2] = -0.01;

        this->spatialParams().setLensCoords(lensLowerLeft_, lensUpperRight_);

// 	ridCreator::grid().globalRefine(1);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C
    /*
      * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     *
     * \param element The DUNE Codim<0> entity which intersects with
     *                the finite volume in question
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The sub control volume index inside the finite
     *               volume geometry
     */
    Scalar referencePressure(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    { return pnRef_; }


    // TODO identify source term
    // for example by reading from a vector that contains the source term for
    // each element
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {
        Scalar Jr = 0.;

        int eIdx = this->model().elementMapper().index(element);

        if ( map_->count(eIdx) != 0)
            {
                for (auto it=map_->equal_range(eIdx).first; it!=map_->equal_range(eIdx).second; ++it){
                    Jr += (*it).second.data[0]* (*it).second.fraction  ;
                }
                 if (useHead)
                values = -Jr / element.geometry().volume();
        else
        values = -Jr / element.geometry().volume();
            }
         else{
             values = 0.0;
         }
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
         initial_(values, globalPos); //(pw ) / 9.81 / 1000.0 * (-100.0);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    using ParentType::neumann;
    void neumann(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        priVars[contiEqIdx] = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param values Storage for all primary variables of the initial condition
     * \param globalPos The position for which the boundary type is set
     */
    void initialAtPos(PrimaryVariables &values,
                 const GlobalPosition &globalPos) const
    {
       initial_(values, globalPos);
    }

      // \}
    void setMap (MapType*map)
    {
        map_ = map;
    }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    void episodeEnd()
    {
        // Start new episode if episode is over
        this->timeManager().startNextEpisode(episodeTime);
        this->timeManager().setTimeStepSize(this->timeManager().timeStepSize());
    }

    bool shouldWriteOutput() const
    {
        return
            this->timeManager().episodeWillBeOver();
    }

    /*!
     * \brief returns source/sink values of the model - used by coupled approaches
     *
     * \param sourceValues The current source values which are returned
     */
    void sourceValues(SolutionVector& sourcevalues)
    {
        int gridsize = this->gridView().size(0);
        sourcevalues.resize(gridsize);

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt)
            {
            int eIdx = this->model().elementMapper().index(*eIt);

            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView(), *eIt);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(*this,
                               *eIt,
                               fvGeometry,
                               false /* oldSol? */);

            PrimaryVariables values;

            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    this->solDependentSource(values,
                                             *eIt,
                                             fvGeometry,
                                             scvIdx,
                                             elemVolVars);
                }
                Scalar vol = eIt->geometry().volume();
            sourcevalues[eIdx][0] = values/1000*vol;
            }
    }


private:
       void initial_(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        Scalar sw = GET_RUNTIME_PARAM(TypeTag, Scalar,BoundaryConditions.Sw);
        Scalar pc =
            MaterialLaw::pc(this->spatialParams().materialLawParams(globalPos),
                            sw);
        Scalar g = this->gravity().two_norm();

        if (useHead)
            values[hIdx] = (-pc)/1000./ g *(100.);
        else
            values[pwIdx] = pnRef_ - pc;
   }

    std::string name_;
    MapType *map_;
    Scalar episodeTime;
    Scalar pnRef_;
    Scalar eps_;
    GlobalPosition lensLowerLeft_;
    GlobalPosition lensUpperRight_;
};
} //end namespace

#endif
