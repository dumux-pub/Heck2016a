// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_COUPLEDROSI_TEST_PROBLEM_HH
#define DUMUX_COUPLEDROSI_TEST_PROBLEM_HH

#include <dumux/modelcoupling/richardsrootsystem/richardsrootsystemproblem.hh>
#include "rootsystemtestproblem.hh"
#include "richardstestproblem.hh"

namespace Dumux
{
template <class TypeTag>
class CoupledRosiTestProblem;

namespace Properties
{
NEW_TYPE_TAG(CoupledRosiTestProblem, INHERITS_FROM(CoupledRichardsRootSystem));

// Set the problem property
SET_TYPE_PROP(CoupledRosiTestProblem, Problem, 
              Dumux::CoupledRosiTestProblem<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(CoupledRosiTestProblem, SubProblem1TypeTag, TTAG(RootsystemTestCCProblem));
SET_TYPE_PROP(CoupledRosiTestProblem, SubProblem2TypeTag, TTAG(RichardsTestCCProblem));

SET_PROP(RootsystemTestProblem, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(CoupledRosiTestProblem), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};

SET_PROP(RichardsTestProblem, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(CoupledRosiTestProblem), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};

}

template <class TypeTag>
class CoupledRosiTestProblem : public CoupledRichardsRootSystemProblem<TypeTag>
{
    typedef CoupledRichardsRootSystemProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // obtain the type tags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridView) GridView2;

public:
    CoupledRosiTestProblem(TimeManager &timeManager, const GridView1 &gridView1, const GridView2 &gridView2)
    : ParentType(timeManager, gridView1, gridView2)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, 
                                             std::string, 
                                             Problem, 
                                             Name);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

   

private:
    std::string name_;
};

} //end namespace

#endif
