// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the RichardsTestProblem
 */
#ifndef DUMUX_RICHARDS_TEST_SPATIAL_PARAMETERS_HH
#define DUMUX_RICHARDS_TEST_SPATIAL_PARAMETERS_HH

#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dune/common/fmatrix.hh>

#include <dumux/porousmediumflow/richards/implicit/model.hh>

namespace Dumux
{

// forward declaration
template<class TypeTag>
class RichardsTestSpatialParamsDGF;

namespace Properties
{
NEW_PROP_TAG(SpatialParams);
// The spatial parameters TypeTag
NEW_TYPE_TAG(RichardsTestSpatialParamsDGF);

// Set the spatial parameters
SET_TYPE_PROP(RichardsTestSpatialParamsDGF, SpatialParams, Dumux::RichardsTestSpatialParamsDGF<TypeTag>);

// Set the material law
SET_PROP(RichardsTestSpatialParamsDGF, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}

/*!
 * \ingroup RichardsModel
 * \ingroup ImplicitTestProblems
 * \brief The spatial parameters for the RichardsTestProblem
 */
template<class TypeTag>
class RichardsTestSpatialParamsDGF : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename Grid::template Codim<0>::Entity::EntitySeed ElementSeed;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimMatrix;
//     typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;


public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    //! The parameters of the material law to be used
    typedef typename MaterialLaw::Params MaterialLawParams;

    /*!
     * \brief Constructor
     *
     * \param gridView The DUNE GridView representing the spatial
     *                 domain of the problem.
     */
    RichardsTestSpatialParamsDGF(const GridView& gridView)
        : ParentType(gridView)

    {  //get spatial parameters from input file, for each value there are two, to simulate heterogeneitiy
       // residual saturations
        materialParams_.setSwr(0.05);
        materialParams_.setSnr(0.0);

        // parameters for the Van Genuchten law
        // alpha and n
        Vgalpha_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.VGAlpha);
        Vgn_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.VGN);
        materialParams_.setVgAlpha(Vgalpha_);
        materialParams_.setVgn(Vgn_);
        VgalphaLens_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.VGAlphaLens);
        VgnLens_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.VGNLens);
        materialParamsLens_.setVgAlpha(VgalphaLens_);
        materialParamsLens_.setVgn(VgnLens_);

        permeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Permeability);
        permeabilityLens_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermeabilityLens);

        porosity_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Porosity);
        porosityLens_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorosityLens);

    }

     /*!
     * \brief Returns the intrinsic permeability tensor [m^2] at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    {
     const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
     if (isInLens_(globalPos))
       return permeabilityLens_;
     else
       return permeability_;
     }

     /*!
     * \brief Returns the porosity [] at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const {
     const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
     if (isInLens_(globalPos))
       return porosityLens_;
     else
       return porosity_;
    }

    /*!
     * \brief Returns the parameters for the material law at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
     const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
     if (isInLens_(globalPos))
       return materialParamsLens_;
     else
       return materialParams_;

    }
    /*!
     * \brief Returns the parameters for the material law at a given location
     *
     * This method is not actually required by the Richards model, but provided
     * for the convenience of the RichardsTestProblem
     *
     * \param globalPos A global coordinate vector
     */
    const MaterialLawParams& materialLawParams(const GlobalPosition &globalPos) const
    {
     if (isInLens_(globalPos))
       return materialParamsLens_;
     else
       return materialParams_;
    }
    /*!
     * \brief Set the bounding box of the low-permeability lens
     *
     * This method is not actually required by the Richards model, but provided
     * for the convenience of the RichardsLensProblem
     *
     * \param lensLowerLeft the lower left corner of the lens
     * \param lensUpperRight the upper right corner of the lens
     */
    void setLensCoords(const GlobalPosition& lensLowerLeft,
                       const GlobalPosition& lensUpperRight)
    {
        lensLowerLeft_ = lensLowerLeft;
        lensUpperRight_ = lensUpperRight;
     }

private:
        bool isInLens_(const GlobalPosition &globalPos) const
        {
        for (int i = 0; i < dimWorld; ++i) {
            if (globalPos[i] < lensLowerLeft_[i] || globalPos[i] > lensUpperRight_[i])
                return false;
              }
                return true;
        }

    GlobalPosition lensLowerLeft_;
    GlobalPosition lensUpperRight_;
    MaterialLawParams materialParams_;
    MaterialLawParams materialParamsLens_;
    Scalar permeability_;
    Scalar permeabilityLens_;
    Scalar porosity_;
    Scalar porosityLens_;
    Scalar Vgalpha_;
    Scalar Vgn_;
    Scalar VgalphaLens_;
    Scalar VgnLens_;
   };

} // end namespace

#endif