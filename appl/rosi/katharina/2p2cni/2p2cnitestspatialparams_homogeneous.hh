// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the TwoPTwoCNITestProblem
 */
#ifndef DUMUX_TWOPTWOCNI_TEST_SPATIAL_PARAMETERS_HH
#define DUMUX_TWOPTWOCNI_TEST_SPATIAL_PARAMETERS_HH

#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/io/ploteffectivediffusivitymodel.hh>
#include <dumux/io/plotmateriallaw.hh>
#include <dumux/io/plotthermalconductivitymodel.hh>


namespace Dumux
{

// forward declaration
template<class TypeTag>
class TwoPTwoCNITestSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(TwoPTwoCNITestSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(TwoPTwoCNITestSpatialParams, SpatialParams, Dumux::TwoPTwoCNITestSpatialParams<TypeTag>);

// Set the material law
SET_PROP(TwoPTwoCNITestSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}

/*!
 * \ingroup TwoPTwoCNIModel
 * \ingroup ImplicitTestProblems
 * \brief The spatial parameters for the TwoPTwoCNITestProblem
 */
template<class TypeTag>
class TwoPTwoCNITestSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
        typedef Dune::FieldVector<int,dimWorld> IntVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    //! The parameters of the material law to be used
    typedef typename MaterialLaw::Params MaterialLawParams;

    /*!
     * \brief Constructor
     *
     * \param gridView The DUNE GridView representing the spatial
     *                 domain of the problem.
     */
    TwoPTwoCNITestSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {
        //get material params from input file
      alpha_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.alpha);
      n_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.n);
       // residual saturations
        materialParams_.setSwr(0.05);
        materialParams_.setSnr(0.0);

        // parameters for the Van Genuchten law
        // alpha and n
        materialParams_.setVgAlpha(alpha_);
        materialParams_.setVgn(n_);

        // get spatial params from input file
        permeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Permeability);
        porosity_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Porosity);


      //domain extents
        Scalar xMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftX);
        Scalar xMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightX);
        Scalar yMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftY);
        Scalar yMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightY);
        Scalar zMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftZ);
        Scalar zMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightZ);
        bBoxMin_[0] = xMin;
        bBoxMax_[0] = xMax;
        bBoxMin_[1] = yMin;
        bBoxMax_[1] = yMax;
        bBoxMin_[2] = zMin;
        bBoxMax_[2] = zMax;
        lengthX_ = bBoxMax_[0] - bBoxMin_[0];
        lengthY_ = bBoxMax_[1] - bBoxMin_[1];
        height_ = bBoxMax_[2] - bBoxMin_[2];

    }

    /*!
     * \brief Returns the intrinsic permeability tensor [m^2] at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    {
        return permeability_;
    }

    /*!
     * \brief Returns the porosity [] at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return porosity_;}

    /*!
     * \brief Returns the parameters for the material law at a given location
     *
     * \param element An arbitrary DUNE Codim<0> entity of the grid view
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
        return materialLawParams(fvGeometry.subContVol[scvIdx].global);
    }

    /*!
     * \brief Returns the parameters for the material law at a given location
     *
     * This method is not actually required by the TwoPTwoCNI model, but provided
     * for the convenience of the TwoPTwoCNITestProblem
     *
     * \param globalPos A global coordinate vector
     */
    const MaterialLawParams& materialLawParams(const GlobalPosition &globalPos) const
    {
        return materialParams_;
    }

        /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidHeatCapacity(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    {
        return 1480; // specific heat capacity of wet soil [J / (kg K)] http://www.engineeringtoolbox.com/specific-heat-capacity-d_391.html
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return 1.6; //  bulk density of sandy soil [kg/m^3]
    }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        return 2; //from engineeringtoolbox (w/m*K)
    }

        /*!
     * \brief Returns the relative position of a global coordinate
     */
    GlobalPosition relativePosition(const GlobalPosition &globalPos) const
    {
        GlobalPosition relativePosition(0.0);
        relativePosition[0] = (globalPos[0] - bBoxMin_[0]) / lengthX_ ;
        relativePosition[1] = (globalPos[1] - bBoxMin_[1]) / lengthY_;
        relativePosition[2] = (globalPos[2] - bBoxMin_[2]) / height_;
        return relativePosition;
    }

    /*!
     * \brief Returns the block ID of a global coordinate
     */
    IntVector blockID(const GlobalPosition &globalPos) const
    {
        GlobalPosition curRelativePosition = relativePosition(globalPos);
        curRelativePosition *= numberOfSoilBlocks_ * 1.0;

        IntVector blockID(0.0);
        blockID[0] = int(floor(curRelativePosition[0]));
        blockID[0] = std::max(blockID[0], 0);
        blockID[0] = std::min(blockID[0], numberOfSoilBlocks_-1);
        blockID[1] = int(floor(curRelativePosition[1]));
        blockID[1] = std::max(blockID[1], 0);
        blockID[1] = std::min(blockID[1], numberOfSoilBlocks_-1);
        return blockID;
    }
   Scalar blockLength(const int blockID) const
    {
        return lengthX_ / numberOfSoilBlocks_;
    }

    /*!
     * \brief Returns the evaporating length ratio of a soil block (for BL model)
     */
    Scalar evaporatingBlockLengthRatio(const int blockID) const
    {
        Scalar curBlockLength = blockLength(blockID);
        Scalar xMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, LowerLeftX);
        Scalar xMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX);
        int cellsX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumberOfCellsX);
        // because the corners are excluded from coupling, substract them from the
        // effective evaprating area for the corner blocks
        if (blockID == 0)
            curBlockLength -= 0.5 * (xMax -xMin) / cellsX;
        if (blockID == numberOfSoilBlocks_-1)
            curBlockLength -= 0.5 * (xMax -xMin) / cellsX;
        return curBlockLength / (lengthX_ / numberOfSoilBlocks_);
    }

private:
    MaterialLawParams materialParams_;
    Scalar permeability_;
    Scalar porosity_;
    Scalar alpha_;
    Scalar n_;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar lengthX_;
    Scalar lengthY_;
    Scalar height_;
    int numberOfSoilBlocks_;

};

} // end namespace

#endif