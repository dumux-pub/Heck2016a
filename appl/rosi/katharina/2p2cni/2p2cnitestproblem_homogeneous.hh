// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the two-phase, two component model
 *
 */
#ifndef DUMUX_TWOPTWOCNI_TEST_PROBLEM_HOMOGENEOUS_HH
#define DUMUX_TWOPTWOCNI_TEST_PROBLEM_HOMOGENEOUS_HH

#include <cmath>

#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/freeflow/boundarylayermodel.hh> //includes models for boundary layer thickness
#include <dumux/freeflow/masstransfermodel.hh> //includes e.g. Schlünder model
#include <dumux/io/gnuplotinterface.hh>

//! get the properties needed for subproblems
#include <dumux/modelcoupling/common/subproblemproperties.hh>

#include <dune/grid/yaspgrid.hh>
//#include <dune/alugrid/grid.hh>

#include "2p2cnitestspatialparams_homogeneous.hh"

#define ISOTHERMAL 0

namespace Dumux
{
template <class TypeTag>
class TwoPTwoCNITestProblemHomogeneous;

namespace Properties
{
NEW_TYPE_TAG(TwoPTwoCNITestProblemHomogeneous, INHERITS_FROM(TwoPTwoCNI, TwoPTwoCNITestSpatialParams));
NEW_TYPE_TAG(TwoPTwoCNITestBoxProblem, INHERITS_FROM(BoxModel, TwoPTwoCNITestProblemHomogeneous));
NEW_TYPE_TAG(TwoPTwoCNITestCCProblem, INHERITS_FROM(CCModel, TwoPTwoCNITestProblemHomogeneous));

SET_TYPE_PROP(TwoPTwoCNITestProblemHomogeneous, Grid, Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3> >);
//SET_TYPE_PROP(TwoPTwoCNITestProblemHomogeneous, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_PROP(TwoPTwoCNITestProblemHomogeneous, Problem)
{ typedef Dumux::TwoPTwoCNITestProblemHomogeneous<TypeTag> type; };

// Set the spatial parameters
SET_PROP(TwoPTwoCNITestProblemHomogeneous, SpatialParams)
{ typedef Dumux::TwoPTwoCNITestSpatialParams<TypeTag> type; };

// Set fluid configuration
SET_TYPE_PROP(TwoPTwoCNITestProblemHomogeneous,
              FluidSystem,
              Dumux::FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                                          Dumux::H2O<typename GET_PROP_TYPE(TypeTag, Scalar)>,
                                          /*useComplexrelations=*/false>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(TwoPTwoCNITestProblemHomogeneous, UseMoles, false);

// Enable gravity
SET_BOOL_PROP(TwoPTwoCNITestProblemHomogeneous, ProblemEnableGravity, true);

// Enable velocity output
SET_BOOL_PROP(TwoPTwoCNITestProblemHomogeneous, VtkAddVelocity, true);

SET_STRING_PROP(TwoPTwoCNITestProblemHomogeneous, GridParameterGroup, "SoilGrid");

SET_INT_PROP(TwoPTwoCNITestProblemHomogeneous,
             Formulation,
             TwoPTwoCFormulation::pnsw);
// The gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(TwoPTwoCNITestProblemHomogeneous, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

}

template <class TypeTag>
class TwoPTwoCNITestProblemHomogeneous : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
     // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
    pressureIdx = Indices::pressureIdx, //for pnsw formulation this is pn
    switchIdx = Indices::switchIdx, //for pnsw this is sw

        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,


        contiH2OEqIdx = Indices::contiWEqIdx,
        contitotalMassEqIdx = Indices::contiNEqIdx,
        conti0EqIdx = Indices::conti0EqIdx, // Index of the mass conservation equation for the first component, here water

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, CouplingTransferData) MapType;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

     enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };

    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
public:
    TwoPTwoCNITestProblemHomogeneous(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gnuplot_(false)
    {
      //domain extents
     Scalar xMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftX);
     Scalar xMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightX);
     Scalar yMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftY);
     Scalar yMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightY);
     Scalar zMin = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, LowerLeftZ);
     Scalar zMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, UpperRightZ);
     bBoxMin_[0] = xMin;
     bBoxMax_[0] = xMax;
     bBoxMin_[1] = yMin;
     bBoxMax_[1] = yMax;
     bBoxMin_[2] = zMin;
     bBoxMax_[2] = zMax;

        int gridsize = this->gridView().size(0);
        evaporationValues_.resize(gridsize);

        std::string coupledName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);
       eps_ = 1e-6; //small number for better match for boundary conditions


        name_ = coupledName + "_SoilProblem3d_2p2cni";

        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                   Scalar,
                                                   TimeManager,
                                                   EpisodeTime);

        this->timeManager().startNextEpisode(episodeTime);// time episode

        FluidSystem::init();

       if(!useMoles)
        {
            std::cout<<"problem uses mass-fractions"<<std::endl;
        }
        else
        {
            std::cout<<"problem uses mole-fractions"<<std::endl;
        }

     }

    //! \brief The destructor
    ~TwoPTwoCNITestProblemHomogeneous()
    {
        storageFile_.close();
    }


      void addOutputVtkFields()
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        unsigned numElements = this->gridView().size(0);
        ScalarField *source = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *EvaporationRate = this->resultWriter().allocateManagedBuffer(numElements);


        for (auto&& element : elements(this->gridView()))
        {
            int eIdx = this->elementMapper().index(element);
            (*source)[eIdx] = 0.0;

            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView(), element);

            if (map_->count(eIdx) != 0) //if element exists in map (has intersection with a 3d element)
            {
                ElementVolumeVariables elemVolVars;
                elemVolVars.update(*this, element, fvGeometry, false /* oldSol? */);

                PrimaryVariables values;
                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                    this->solDependentSource(values, element, fvGeometry, scvIdx, elemVolVars);

                (*source)[eIdx] = values[contiH2OEqIdx];
             }

      (*EvaporationRate)[eIdx]= evaporationValues_[eIdx];
}
        // attach data to the vtk output
       this->resultWriter().attachCellData(*source, "source");
       this->resultWriter().attachCellData(*EvaporationRate, "EvaporationRate");


    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }
#if ISOTHERMAL
    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperature() const
    { return 273.15 + 10; }//temperature_; }
#endif

    // TODO identify source term
    // for example by reading from a vector that contains the source term for
    // each element
     void solDependentSource(PrimaryVariables & values,
                            const Element & element,
                            const FVElementGeometry & fvGeometry,
                            const int scvIdx,
                            const ElementVolumeVariables &  elemVolVars) const
     {
        Scalar Jr = 0.;
        int eIdx = this->model().elementMapper().index(element);

        if ( map_->count(eIdx) != 0) {
            for (auto it=map_->equal_range(eIdx).first; it!=map_->equal_range(eIdx).second; ++it)
            {
               Jr += (*it).second.data* (*it).second.fraction  ;
            }
         //mass balance in kg/(m^3*s)
         values[contiH2OEqIdx] = -Jr / element.geometry().volume();
         values[contitotalMassEqIdx] = values[contiH2OEqIdx];
         //energy balance, (enthalpy in J/kg) is then J/m^3*s
         values[energyEqIdx]= 0.0;
         values[energyEqIdx]  += elemVolVars[scvIdx].enthalpy(wPhaseIdx)
                                  * values[contiH2OEqIdx];
        }
        else
             values= 0.0;
     }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
      values.setAllNeumann();
      if(globalPos[2] > this->bBoxMax()[2] - eps_ ) // neumann for the top except for pressureN and totalMass
      {
            values.setAllNeumann();
            values.setDirichlet(pressureIdx,contitotalMassEqIdx);
      }

    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
      values[pressureIdx]=1e5;
      values[switchIdx]=0.2;

#if !ISOTHERMAL
      values[temperatureIdx] = 293;
#endif
    }

      /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
       void solDependentNeumann(PrimaryVariables &values,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const Intersection &intersection,
                        const int scvIdx,
                        const int boundaryFaceIdx,
                        const ElementVolumeVariables &elemVolVars) const
      {
           values = 0; //set zero for  sides

        // assume thermal conduction through the plexiglass box
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar temperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperaturePM);
        Scalar temperatureInside = elemVolVars[scvIdx].temperature();
        values[energyEqIdx] += plexiglassThermalConductivity
                               *(temperatureInside - temperatureRef)
                               /plexiglassThickness;

        GlobalPosition globalPos;
        if (isBox)
            globalPos = element.geometry().corner(scvIdx);
        else
            globalPos = intersection.geometry().center();

// implementation of penman's equation to calculate evaporation rate
//   Scalar moleFracW = elemVolVars[scvIdx].moleFraction(nPhaseIdx, wCompIdx);
//   Scalar pressureN = elemVolVars[scvIdx].pressure(nPhaseIdx);
//   Scalar SoilVaporPressure = moleFracW*pressureN;
//   Scalar AirVaporPressure = 1200; //pa saturation pressure 2310 Pa at 20°C
//   Scalar slope= 144.7; //pa/°C for 20°C
//   Scalar Rn = 88.16; //w/m²
//   Scalar SoilHeatflux = 3.83; //W/m²
//   Scalar densityAir= 1.205; //kg/m³ at 20°C
//   Scalar heatCapacityAir= 1005; //J/kg*K at 20°C
//   Scalar windSpeed=2.4; //m/s
//   Scalar latentHeat=2450; //J/kg for 20°C
//   Scalar psychrometricC= 66;
//   Scalar fu = (slope*(Rn-SoilHeatflux)+densityAir*heatCapacityAir*windSpeed)/(latentHeat*(slope+psychrometricC));
//   Scalar EvaporationRate = fu*(SoilVaporPressure-AirVaporPressure); //kg/(m^2*s)

          // implementation of boundary layer model
            Scalar moleFracInside = elemVolVars[scvIdx].moleFraction(nPhaseIdx, wCompIdx);
            Scalar moleFracRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
            Scalar velocity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
            Scalar distance = globalPos[0]
                              + GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SoilGrid, RunUpDistanceX1)
                              + GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, Offset);
            Scalar kinematicViscosity = elemVolVars[scvIdx].viscosity(nPhaseIdx) / elemVolVars[scvIdx].density(nPhaseIdx);
            // initialize and run the boundary layer model
            unsigned blModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, BoundaryLayer, Model);
            BoundaryLayerModel<TypeTag> blmodel(velocity, distance, kinematicViscosity, blModel);
            if (blModel == 1)
                blmodel.setConstThickness(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, ConstThickness));
            if (blModel >= 5)
                blmodel.setRoughnessLength(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, RoughnessLength));
            blmodel.setYPlus(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, YPlus));
            Scalar massBLThickness = blmodel.massBoundaryLayerThickness();
            Scalar thermalBLThickness = blmodel.thermalBoundaryLayerThickness();

         //implementation of mass- transfer model, here only three possible...can be adapted
           unsigned mtModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, MassTransfer, Model);
           Scalar massTransferCoefficient = 1.0;
            if (mtModel != 0)
            {
                 MassTransferModel<TypeTag> massTransferModel(elemVolVars[scvIdx].saturation(wPhaseIdx),
                                                             elemVolVars[scvIdx].porosity(),
                                                             massBLThickness, mtModel);
                 if (mtModel == 3)
                     massTransferModel.setCapillaryPressure(elemVolVars[scvIdx].capillaryPressure());

                  if (mtModel == 2 || mtModel == 4)
                      massTransferModel.setCharPoreRadius(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, MassTransfer, CharPoreRadius));

           massTransferCoefficient = massTransferModel.massTransferCoefficient();
           }
           // calculate fluxes
           Scalar diffusiveMoleFluxWater =  massTransferCoefficient
                                            *elemVolVars[scvIdx].diffCoeff(nPhaseIdx) // tortuosity is not included, pm requires phaseIdx
                                            * (moleFracInside - moleFracRef)
                                            /massBLThickness
                                            * elemVolVars[scvIdx].molarDensity(nPhaseIdx);
          Scalar EvaporationRate = diffusiveMoleFluxWater * FluidSystem::molarMass(wCompIdx);
//       diffusiveMassFluxWater = 10.0 /*mm/d*/ / 86400 /*s/d*/ / 1000 /*mm/m*/ * 1000 /*kg/m^3*/;// diffusiveMassFluxWater;

          int eIdx = this->model().elementMapper().index(element); //method walks over all elements..not necessary to loop again
          evaporationValues_[eIdx]=0.0;

           //top for evaporation
           if (globalPos[2] > this->bBoxMax()[2] - eps_) {
               values[contiH2OEqIdx] = EvaporationRate;
               values[energyEqIdx] = 0.0;
               values[energyEqIdx] += FluidSystem::componentEnthalpy(elemVolVars[scvIdx].fluidState(), nPhaseIdx, wCompIdx)*EvaporationRate;
               values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvIdx].fluidState(), nPhaseIdx)*(temperatureInside - temperatureRef)/thermalBLThickness;
            //stores evap values for output
            evaporationValues_[eIdx] = values[contiH2OEqIdx]*0.575*0.575/100/100; //to convert in kg/s times area (0,575 cm)	   }
        }
      }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param values
     * for all primary variables of the initial condition
     * \param globalPos The position for which the boundary type is set
     */
    void initialAtPos(PrimaryVariables &values,
                 const GlobalPosition &globalPos) const
    {
      // values[pressureIdx] = -3. * 9.81 * 1000.0 ; // 3 m
       values=0;
       values[switchIdx]=0.2;
       values[pressureIdx]= 1e5;

#if !ISOTHERMAL
       values[temperatureIdx] = 293; //20 degree
#endif
    }

    void setMap (MapType*map)
    {
        map_ = map;
    }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    void episodeEnd()
    {
        // Start new episode if episode is over
        this->timeManager().startNextEpisode(episodeTime);
        this->timeManager().setTimeStepSize(this->timeManager().timeStepSize());
    }

    bool shouldWriteOutput() const
    {
        return
            this->timeManager().episodeWillBeOver();
    }

    /*!
     * \brief returns source/sink values of the model - used by coupled approaches
     *
     * \param sourceValues The current source values which are returned
     */
    void sourceValues(SolutionVector& sourcevalues)
    {
        int gridsize = this->gridView().size(0);

       sourcevalues.resize(gridsize);

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt)
            {
            int eIdx = this->model().elementMapper().index(*eIt);

            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView(), *eIt);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(*this,
                               *eIt,
                               fvGeometry,
                               false /* oldSol? */);

            PrimaryVariables values;

            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    this->solDependentSource(values,
                                             *eIt,
                                             fvGeometry,
                                             scvIdx,
                                             elemVolVars);
                }
            sourcevalues[eIdx][0] = values[wPhaseIdx];
            }
    }


    /*!
     * \brief Returns the initial phase state for a control volume.
     *
     * \param vertex The vertex
     * \param vIdxGlobal The global index of the vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vertex,
                             int &vIdxGlobal,
                             const GlobalPosition &globalPos) const
    { return Indices::bothPhases; }

    void postTimeStep()
    {   }

private:
    std::string name_;
    MapType *map_;
    Scalar episodeTime;
    Scalar eps_;
    mutable std::vector<Scalar> evaporationValues_;

    int numberOfSoilBlocks_;

    mutable Scalar viscousBLThicknessLeft_;
    mutable Scalar viscousBLThicknessRight_;

    Scalar initializationTime_;
    std::ofstream storageFile_;
    std::vector<std::string> fluxFileNames_;
    Dumux::GnuplotInterface<double> gnuplot_;

    mutable std::vector<GlobalPosition> position_;
    mutable std::vector<Scalar> waterFlux_;
    mutable std::vector<Scalar> energyFlux_;

    int freqRestart_;
    int freqOutput_;
    int freqMassOutput_;
    int freqVaporFluxOutput_;

    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;

    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;

};
} //end namespace

#endif
