// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_ROOTSYSTEM_TEST_PROBLEM_HH
#define DUMUX_ROOTSYSTEM_TEST_PROBLEM_HH

#include <dumux/implicit/rootsystem/rootsystemmodel.hh>
#include <dumux/implicit/rootsystem/rootsystemproblem.hh>

#include <dumux/implicit/cellcentered/variableccfvelementgeometry.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

//! get the properties needed for subproblems
#include <dumux/modelcoupling/common/subproblemproperties.hh>

#include <dumux/linear/seqsolverbackend.hh>

#include "rootsystemtestspatialparams.hh"

namespace Dumux
{
template <class TypeTag>
class RootsystemTestProblem;

namespace Properties
{
NEW_TYPE_TAG(RootsystemTestProblem, INHERITS_FROM(Rootsystem));
NEW_TYPE_TAG(RootsystemTestBoxProblem, INHERITS_FROM(BoxModel, RootsystemTestProblem));
NEW_TYPE_TAG(RootsystemTestCCProblem, INHERITS_FROM(CCModel, RootsystemTestProblem));

SET_PROP(RootsystemTestProblem, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};

// Set the grid type
SET_PROP(RootsystemTestProblem, Grid)
{
    typedef Dune::FoamGrid<1, 3> type;
};

// Set the problem property
SET_PROP(RootsystemTestProblem, Problem)
{ typedef Dumux::RootsystemTestProblem<TypeTag> type; };

// Set the spatial parameters
SET_PROP(RootsystemTestProblem, SpatialParams)
{ typedef Dumux::RootsystemTestSpatialParams<TypeTag> type; };

// Linear solver settings
#if HAVE_UMFPACK
SET_TYPE_PROP(RootsystemTestProblem, LinearSolver, UMFPackBackend<TypeTag>);
#endif

// Set the fv geometry for network grids
SET_TYPE_PROP(RootsystemTestProblem, FVElementGeometry, Dumux::VariableCCFVElementGeometry<TypeTag>);


// Enable gravity
SET_BOOL_PROP(RootsystemTestProblem, ProblemEnableGravity, false);

// write newton convergence to vtk
SET_BOOL_PROP(RootsystemTestProblem, NewtonWriteConvergence, false);

SET_BOOL_PROP(RootsystemTestProblem, GrowingGrid, false);
}

/*!
 * \ingroup OnePBoxModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::SGrid<2,2> type;</tt> to
 * <tt>typedef Dune::SGrid<3,3> type;</tt> in the problem file
 * and use <tt>1p_3d.dgf</tt> in the parameter file.
 */
template <class TypeTag>
class RootsystemTestProblem : public RootsystemProblem<TypeTag>
{
    typedef RootsystemProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pIdx = Indices::pIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, CouplingTransferData) MapType;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

public:
    RootsystemTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        std::string coupledName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        name_ = coupledName + "_rootProblem1d";

        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                                   Scalar,
                                                   TimeManager,
                                                   EpisodeTime);
        this->timeManager().startNextEpisode(episodeTime);// time episode

//         GridCreator::grid().globalRefine(1);

        this->spatialParams().setParams();

        pInit.resize(gridView.size(0));
    }
    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C


    // \}
    /*!
     * \name Boundary conditions
     */
    // \{
    void boundaryTypes(BoundaryTypes &values,
                        const Intersection &intersection) const
     {

    int vertexIdx = this->vertexMapper().subIndex(intersection.inside(), intersection.indexInInside(), dim);

        if ( vertexIdx == 0 ){
            if (this->getWaterStress()){
                std::cout <<"Stress at root collar! "<< std::endl;
                values.setAllDirichlet();
            }
           else {
                std::cout <<"neumann bc"<< std::endl;
                values.setAllNeumann();

                }
            }
         else
            values.setAllNeumann();


     }
//   void boundaryTypesAtPos (BoundaryTypes &values,
//                        const GlobalPosition &globalPos ) const
//     {
//
//          // if (globalPos[2] + eps_ > 0 )
//          //     values.setAllDirichlet();
//          // else
//             values.setAllNeumann();
//     }


    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        if (globalPos[2] + eps_ > 0 )
              values[pIdx] = GET_RUNTIME_PARAM(TypeTag,
                                               Scalar,
                                               BoundaryConditions.CriticalCollarPressure);

          //values[pIdx] *= 1.01;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
     // void neumannAtPos(PrimaryVariables &values,
     //             const GlobalPosition &globalPos) const

    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        //if (globalPos[2] + eps_ > 0 ){
        if (globalPos[2] > -0.00029-eps_ )
       {
            values = GET_RUNTIME_PARAM(TypeTag,
                                       Scalar,
                                       BoundaryConditions.TranspirationRate);
        }
        else
            values = 0.0;

    }

    /*void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        int vertexIdx = this->vertexMapper().subIndex(*intersection.inside(), intersection.indexInInside(), dim);
        //std::cout <<"boundaryFaceIdx: "<<boundaryFaceIdx  <<std::endl;
        //std::cout <<"vertexIdx: "<<vertexIdx  <<std::endl;

        if ( vertexIdx == 0 )
            values = GET_RUNTIME_PARAM(TypeTag,
                                       Scalar,
                                       BoundaryConditions.TranspirationRate);
        else
            values = 0.0;

            }*/
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        priVars[pIdx] =  -8e5;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in
     units of \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scvIdx The local subcontrolvolume index
     * \param elemVolVars All volume variables for the element
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &values,
                     const Element &element,
                     const FVElementGeometry &fvGeometry,
                     const int scvIdx,
                     const ElementVolumeVariables &elemVolVars) const
    {
        int eIdx = this->model().elementMapper().index(element);

        const SpatialParams &spatialParams = this->spatialParams();
        Scalar Kr = spatialParams.Kr(element, fvGeometry, scvIdx);
        Scalar rootSurface = spatialParams.rootSurface(element, fvGeometry, scvIdx);
        Scalar phx = elemVolVars[0].pressure();
        Scalar phs = 0.0;

        if ( map_->count(eIdx) != 0)
            {
                for (auto it=map_->equal_range(eIdx).first; it!=map_->equal_range(eIdx).second; ++it){
                    phs += (*it).second.data[0] * (*it).second.fraction;
                }
                // sink defined as radial flow Jr [m^3 s-1] per root volume [m^3]
                values = Kr * rootSurface * (phs - phx) / element.geometry().volume()*1000;
            }
        else
            values = 0.0;

        }

    void setMap (MapType *map)
    {
        map_ = map;
    }

    void getMap (MapType *map)
    {
        map = map_;
    }

    void episodeEnd()
    {
        // Start new episode if episode is over
        this->timeManager().startNextEpisode(episodeTime);
        this->timeManager().setTimeStepSize(this->timeManager().timeStepSize());
    }

    bool shouldWriteOutput() const
    {
        return
            this->timeManager().episodeWillBeOver() ;
    }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

private:
    std::string name_;

    MapType *map_;
    Scalar sinkSum;
    const Scalar eps_ = 1e-5;
    Scalar episodeTime;
    SolutionVector pInit;
};
} //end namespace

#endif
