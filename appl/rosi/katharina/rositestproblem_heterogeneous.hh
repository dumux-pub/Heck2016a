// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Doc me!
 */
#ifndef DUMUX_ROSI_TEST_PROBLEM_HH
#define DUMUX_ROSI_TEST_PROBLEM_HH

#include <dumux/multidimension/common/multidimensionproblem.hh>
#include <dumux/multidimension/1d3d/1d3dproperties.hh>

#include "rootsystemtestproblem.hh"
#include "richardstestproblem_heterogeneous.hh"

namespace Dumux
{
template <class TypeTag>
class RosiTestProblemHeterogeneous;

namespace Properties
{
NEW_TYPE_TAG(RosiTestProblemHeterogeneous, INHERITS_FROM(OneDThreeDModel));

// Set the problem property
SET_TYPE_PROP(RosiTestProblemHeterogeneous, Problem,
              Dumux::RosiTestProblemHeterogeneous<TypeTag>);
//
// Set the two sub-problems of the global problem
SET_TYPE_PROP(RosiTestProblemHeterogeneous, SubProblem1TypeTag, TTAG(RootsystemTestCCProblem));
SET_TYPE_PROP(RosiTestProblemHeterogeneous, SubProblem2TypeTag, TTAG(RichardsTestCCProblem));

SET_TYPE_PROP(RosiTestProblemHeterogeneous, DataContainer1, std::array<double,1>);
SET_TYPE_PROP(RosiTestProblemHeterogeneous, DataContainer2, std::array<double,2>);

SET_PROP(RootsystemTestProblem, CouplingTransferData)
{
private:
    typedef typename GET_PROP(TTAG(RosiTestProblemHeterogeneous), DataContainer1) DataContainer;
    typedef typename DataContainer::type DataContainerType;
public:
    typedef OneDThreeDMap<DataContainerType> type;
};

SET_PROP(RichardTestProblemHeterogeneous, CouplingTransferData)
{
private:
    typedef typename GET_PROP(TTAG(RosiTestProblemHeterogeneous), DataContainer2) DataContainer;
    typedef typename DataContainer::type DataContainerType;
public:
    typedef OneDThreeDMap<DataContainerType> type;
};

SET_PROP(RootsystemTestProblem, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(RosiTestProblemHeterogeneous), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }


    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};

SET_PROP(RichardTestProblemHeterogeneous, ParameterTree)
{private:
    typedef typename GET_PROP(TTAG(RosiTestProblemHeterogeneous), ParameterTree) ParameterTree;
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }

};

    SET_BOOL_PROP(RichardTestProblemHeterogeneous, UseHead, false);
}

template <class TypeTag>
class RosiTestProblemHeterogeneous : public MultiDimensionProblem<TypeTag>
{
    typedef MultiDimensionProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // obtain the type tags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridView) GridView1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridView) GridView2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, SolutionVector) SolutionVectorRoot;
    typedef typename GET_PROP_TYPE(SubTypeTag2, SolutionVector) SolutionVectorMatrix;

    typedef typename GET_PROP_TYPE(SubTypeTag1, TimeManager) SubTimeManager1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, TimeManager) SubTimeManager2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, Problem) SubProblem1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Problem) SubProblem2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, CouplingTransferData) MapType1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, CouplingTransferData) MapType2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, VolumeVariables) VolumeVariables1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, VolumeVariables) VolumeVariables2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, Indices) Indices1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Indices) Indices2;


public:
    RosiTestProblemHeterogeneous(TimeManager &timeManager, const GridView1 &gridView1, const GridView2 &gridView2)
        : ParentType(timeManager, gridView1, gridView2), gridView1_(gridView1), gridView2_(gridView2),
          subProblem1_(subTimeManager1_, gridView1),
          subProblem2_(subTimeManager2_, gridView2)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        episodeTime = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeTime);

        this->timeManager().startNextEpisode(episodeTime);

    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

  /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {
        updateMapRoot();
        updateMapSoil();

        preSolRoot = this->subProblem1().model().curSol();
        preSolSoil = this->subProblem2().model().curSol();

    }

 /*!
     * \brief Called by Dumux::TimeManager in order to do a time
     *        integration on the model.
     */
    void timeIntegration()
    {
           Scalar i = 0;
           bool saturationisok = false;

        this->timeManager().startNextEpisode(episodeTime);
        this->subTimeManager1().setTime(this->timeManager().time());
        this->subTimeManager1().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        this->subTimeManager1().setTimeStepSize(this->subTimeManager1().previousTimeStepSize());

        this->subTimeManager2().setTime(this->timeManager().time());
        this->subTimeManager2().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        this->subTimeManager2().setTimeStepSize(this->subTimeManager2().previousTimeStepSize());


      while (!saturationisok)

      {
        this->subProblem1().model().prevSol() = preSolRoot;
        this->subProblem2().model().prevSol() = preSolSoil;

        std::cout << "coupled timeIntegration t = " << this->timeManager().time() << std::endl;
        bool reIterate = true;

        SolutionVectorRoot sourceRoot;
        SolutionVectorRoot sourceRoot2;
        SolutionVectorRoot diffRoot;
        Scalar normRoot;

        SolutionVectorMatrix sourceSoil;
        SolutionVectorMatrix sourceSoil2;
        SolutionVectorMatrix diffSoil;
        Scalar normSoil;

        Scalar eps = 1e-10;
        int itNum = 0;

        // update root source
        updateMapRoot();
        // run first model -> Root
        this->subTimeManager1().setTime(this->timeManager().time());
        this->subTimeManager1().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        this->subTimeManager1().setTimeStepSize(this->subTimeManager1().previousTimeStepSize());
        std::cout << "SOLVE ROOT " << std::endl;
        this->subTimeManager1().run();
        // get root source values
        this->subProblem1().model().sourceValues(sourceRoot);

        // update soil source
        updateMapSoil();
        // run second model -> Soil
        this->subTimeManager2().setTime(this->timeManager().time());
        this->subTimeManager2().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
        this->subTimeManager2().setTimeStepSize(this->subTimeManager2().previousTimeStepSize());
        std::cout << "SOLVE SOIL " << std::endl;
        this->subTimeManager2().run();
        // get soil source values
        updateMapRoot();
        updateMapSoil();
        this->subProblem2().sourceValues(sourceSoil);

        while (reIterate)
        {
                normRoot = 0.0;
                normSoil = 0.0;

                //update and re-run root model
                updateMapRoot();
                std::cout << "SOLVE ROOT " << std::endl;
                this->subTimeManager1().run();

                // get root source values
                this->subProblem1().model().sourceValues(sourceRoot2);

                // calc norm of root sink between the last iteration steps
                diffRoot.resize(sourceRoot2.size());
                for (int j = 0; j < int(sourceRoot2.size()); ++j) {
                     if (sourceRoot[j][0] != 0 &&  sourceRoot2[j][0] != 0) {
                    diffRoot[j][0] = (sourceRoot[j][0] - sourceRoot[j][0]);
                    normRoot += std::abs(diffRoot[j][0]) * std::abs(diffRoot[j][0]);
                     }
                }
                if (normRoot!= 0)
                     normRoot = std::sqrt(normRoot);
                std::cout <<"Norm Root: " << normRoot  << std::endl;

                //update and re-run soil model
                updateMapSoil();
                std::cout << "SOLVE SOIL " << std::endl;
                this->subTimeManager2().run();

                // get soil source values
                this->subProblem2().sourceValues(sourceSoil2);

                // calc norm of soil sink between the last iteration steps
                diffSoil.resize(sourceSoil2.size());
                for (int j = 0; j < int(sourceSoil2.size()); ++j) {
                    if (sourceSoil[j][0] != 0 &&  sourceSoil2[j][0] != 0) {
                        diffSoil[j][0] = (sourceSoil[j][0] - sourceSoil2[j][0]);
                        normSoil +=  std::abs(diffSoil[j][0]) *  std::abs(diffSoil[j][0]);
                    }
                }
                if (normSoil!= 0)
                    normSoil = std::sqrt(normSoil);
                std::cout <<"Norm Soil: " << normSoil  << std::endl;

                // if both norms are smaller eps continue with next time step
                if (normSoil < eps && normRoot < eps){
                    reIterate = false;
                }
                // if not
                else
                    {
                        //update source terms
                        sourceSoil = sourceSoil2;
                        sourceRoot = sourceRoot2;
                        //count interation
                        itNum ++;
                        std::cout <<"Reiterate Models: " << itNum  << std::endl;
                    }

            }
             //get saturation from soil
            //walk over all 3d elements
            Scalar sum = 0;
            Scalar sum1 = sum;
            Scalar saturationSoil = 0;
            Scalar saturationSoil1 = saturationSoil;
            const SolutionVectorMatrix& solSoil = this->subProblem2().model().curSol();
            for(auto&& element : elements(gridView2_))
            {
                int eIdx = this->subProblem2().elementMapper().index(element);

                FVElementGeometry2 fvGeometry;
                fvGeometry.updateInner(element);
                VolumeVariables2 volVars;
                volVars.update(solSoil[eIdx],this->subProblem2(), element, fvGeometry, 0, /*isOldSol=*/false);
                saturationSoil = volVars.saturation(Indices2::wPhaseIdx);
                //  std::cout<<saturationSoil<<"soil saturation"<<std::endl;
            if (saturationSoil <0){
                std::cout<<saturationSoil<<"soil saturation"<<std::endl;
                sum += 1;

            }
        }

            if (sum==0){
                saturationisok=true;
                    std::cout<<"saturationisoktrue"<<saturationisok<<std::endl;
            }
        else{

            std::cout<<"saturationisokfalse"<<saturationisok<<std::endl;
            i = i+1;
                std::cout<<"i"<<i<<std::endl;


            this->timeManager().setTimeStepSize(this->subTimeManager1().previousTimeStepSize()/2);
            this->subTimeManager1().setTime(this->timeManager().time());
            this->subTimeManager1().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
            this->subTimeManager1().setTimeStepSize(this->subTimeManager1().previousTimeStepSize()/2);


            this->subTimeManager2().setTime(this->timeManager().time());
            this->subTimeManager2().setEndTime(this->timeManager().time() + this->timeManager().timeStepSize());
            this->subTimeManager2().setTimeStepSize(this->subTimeManager2().previousTimeStepSize()/2);
        }
        if (i >9 ){
            DUNE_THROW(NumericalProblem,
                           "saturation is too low ");
        }
            sum = sum1;
            saturationSoil = saturationSoil1;

      }
    }//end time integration

    // update the Root- multimap (used by subProblem1 = RootSystemProblem)
    void updateMapRoot()
    {
        // get soil pressure values from the current solution
        const SolutionVectorMatrix& solSoil = this->subProblem2().model().curSol();

        for(auto&& element : elements(gridView2_))
        {
            //get the 3d element index
            int eIdx = this->subProblem2().elementMapper().index(element);
            // walk over the map
            for (auto&& mapIt : this->oneDMap())
            {
                // if the 3d element matches calculate and attach the data
                if(eIdx == mapIt.second.elementIdx)
                {
                mapIt.second.data[0] = solSoil[mapIt.second.elementIdx][0]; //pressure

                  //get saturation from soil
                    FVElementGeometry2 fvGeometry;
                    fvGeometry.updateInner(element);
                    VolumeVariables2 volVars;
                    volVars.update(solSoil[mapIt.second.elementIdx], this->subProblem2(), element, fvGeometry, 0, /*isOldSol=*/false);
                    mapIt.second.data[1] = volVars.saturation(Indices2::wPhaseIdx);
                 }
            }
        }

//         //write pressure values into multimap
//         for (auto it1=this->oneDMap().begin(); it1 != this->oneDMap().end(); ++it1)
//             {
//                 (*it1).second.data = solSoil[(*it1).second.elementIdx];
//             }

        this->subProblem1().setMap(&this->oneDMap());
    }

    // update the Soil- multimap (used by subProblem2 = RichardsProblem)
    void updateMapSoil()
    {
        //get source values from 1D RootProbelm
        SolutionVectorRoot sourceRoot;
        this->subProblem1().model().sourceValues(sourceRoot);

        // iterate over all 1d elements
        for(auto&& element : elements(gridView1_))
        {
            //get the 1d element index
            int eIdx = this->subProblem1().elementMapper().index(element);
            // walk over the 3d map
            for (auto&& mapIt : this->threeDMap())
            {
                // if the 1d element matches calculate and attach the data
                if(eIdx == mapIt.second.elementIdx)
                {
                  mapIt.second.data[0] = sourceRoot[mapIt.second.elementIdx][0];
                }
        }
    }
//         //write source values into multimap
//         for (auto it=this->threeDMap().begin(); it !=this-> threeDMap().end(); ++it)
//             {
//                 (*it).second.data[0] = sourceRoot[(*it).second.elementIdx][0];
//             }
        this->subProblem2().setMap(&this->threeDMap());
    }

    /*!
     * \brief Returns a reference to subproblem1
     */
    SubProblem1 &subProblem1()
    { return subProblem1_; }

    /*!
     * \brief Returns a const reference to subproblem1
     */
    const SubProblem1 &subProblem1() const
    { return subProblem1_; }

    /*!
     * \brief Returns a reference to subproblem2
     */
    SubProblem2 &subProblem2()
    { return subProblem2_; }

    /*!
     * \brief Returns a const reference to subproblem2
     */
    const SubProblem2 &subProblem2() const
    { return subProblem2_; }

    const SubTimeManager1 &subTimeManager1() const
    { return subTimeManager1_; }

    SubTimeManager1 &subTimeManager1()
    { return subTimeManager1_; }

    const SubTimeManager2 &subTimeManager2() const
    { return subTimeManager2_; }

    SubTimeManager2 &subTimeManager2()
    { return subTimeManager2_; }

    /*!
     * \brief Returns a reference to the multimap of 1d data to 3d data
     */
    MapType1 &oneDMap()
    { return this->map1(); }

    /*!
     * \brief Returns a reference to the multimap of 3d data to 1d data
     */
    MapType2 &threeDMap()
    { return this->map2(); }

private:

    std::string name_;
    Scalar episodeTime;

    SolutionVectorRoot preSolRoot;
    SolutionVectorMatrix preSolSoil;

    // for the iterative algorithm
    SubTimeManager1 subTimeManager1_;
    SubTimeManager2 subTimeManager2_;

    GridView1 gridView1_;
    GridView2 gridView2_;

    SubProblem1 subProblem1_;
    SubProblem2 subProblem2_;
};

} //end namespace

#endif
